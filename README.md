### What is this repository for? ###

This is a frozen version of VIgR, a javascript/d3 frontend that provides a visualisation enviroment for immunoglobulin repertoires. Visualisations can be connected to arbitrary data source formats via user-defined adapters


### Version ###

For the ongoing development version, see the [CharlesBird/VigrOngoing](https://bitbucket.org/CharlesBird/vigrongoing/overview) fork

### How do I get set up? ###

Requirements

- To run the local server, Python 3.x

- A modern browser, with javascript enabled

Pull the VIGB-1_dev_spike from the bitbucket repository

Run python/vigr-servedata.py

By default, this expects to find pipeline and any results of standalone analytics
in pipeline_results arranged as study/sample/file
At the moment, it will parse .vidjil files for diversity measures

Open the frontend, html/vigr-flexbox.html in a browser.

### Who do I talk to? ###

Email vigr.developers@gmail.com with questions