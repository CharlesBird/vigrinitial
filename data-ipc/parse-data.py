#
# basic interprocess communication parsing (analytics to visualisation engine) 
#

## this is the baseline data communication
## should run as a class to allow instantiation of different connectors

# standard libraries
import json

# external dependencies
import requests

cap_src = "http://localhost:8000/capabilities"

capJSON = requests.get( cap_src, stream=False)
for raw_cap in capJSON.iter_lines():
    if raw_cap:
        cap_list = json.loads(raw_cap.decode('utf-8'))
        # the decode is needed in py3 to handle the .loads reqt for a str type (vs byte in 2.6+)
        print (cap_list)
        for k in cap_list.keys():
            print ("key: ",k, ", value: ",cap_list[k])
           
