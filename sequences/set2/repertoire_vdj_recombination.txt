antibody_1	IGHV3-66*01;IGHD3-22*01;IGHJ6*03
antibody_2	IGHV3-66*01;IGHD3-22*01;IGHJ6*03
antibody_3	IGHV3-66*01;IGHD3-22*01;IGHJ6*03
antibody_4	IGHV3-66*01;IGHD3-22*01;IGHJ6*03
antibody_5	IGHV3-66*01;IGHD3-22*01;IGHJ6*03
antibody_6	IGHV3-13*01;IGHD3-3*02;IGHJ4*02
antibody_7	IGHV4-30-4*02;IGHD4-17*01;IGHJ4*03
antibody_8	IGHV3-13*02;IGHD3-10*01;IGHJ5*01
antibody_9	IGHV4-39*01;IGHD4-4*01;IGHJ4*01
antibody_10	IGHV2-5*07;IGHD4-4*01;IGHJ4*02
antibody_11	IGHV2-5*07;IGHD4-4*01;IGHJ4*02
antibody_12	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_13	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_14	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_15	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_16	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_17	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_18	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_19	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_20	IGHV3-48*02;IGHD3-16*02;IGHJ6*02
antibody_21	IGHV4-28*02;IGHD3-10*01;IGHJ6*03
antibody_22	IGHV4-28*02;IGHD3-10*01;IGHJ6*03
antibody_23	IGHV4-28*02;IGHD3-10*01;IGHJ6*03
antibody_24	IGHV3-15*08;IGHD3-10*01;IGHJ3*01
antibody_25	IGHV4-28*03;IGHD3-9*01;IGHJ4*03
antibody_26	IGHV4-28*03;IGHD3-9*01;IGHJ4*03
antibody_27	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_28	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_29	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_30	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_31	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_32	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_33	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_34	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_35	IGHV3-33*05;IGHD4-4*01;IGHJ5*02
antibody_36	IGHV3-30*05;IGHD4-17*01;IGHJ2*01
antibody_37	IGHV3-11*05;IGHD3-10*02;IGHJ5*01
antibody_38	IGHV4-59*01;IGHD2-2*03;IGHJ4*02
antibody_39	IGHV4-59*01;IGHD2-2*03;IGHJ4*02
antibody_40	IGHV7-4-1*01;IGHD3-22*01;IGHJ2*01
