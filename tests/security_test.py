#
# security module tests
# 6 Apr 2016, Charles Bird
#

# remember the test_ prefix is mandatory

import unittest
import sys
import os

from security import security as sy

class TestPWcomplexity(unittest.TestCase):    
    @classmethod
    def setUpClass(cls):
        pass
        
    def setUp(self):
        pass

## there seems to be something buggy about unittest access
## to instance variables, so kludges to be found below

    def test_default_contructor(self):
        pc = sy.PWcomplexity()
        min_l = pc._min_len
        self.assertEqual(min_l, sy.PWdefaults.MIN_LEN)
        ch_req = pc._char_req
        expected = {}
        # make sure test doesn't get broken by accident
        for chr_cls in ('u', 'l', 'd', 's'):
            expected[chr_cls] = sy.PWdefaults.MIN_CHAR[chr_cls]
        expected['permute'] = sy.PWdefaults.PERMUTE
        self.assertEqual(ch_req, expected)

    def test_setting_valid_minimum_lengths(self):
        for ml in (4, sy.PWdefaults.MIN_LEN, 50, sy.PWdefaults.MAX_LEN ):
            with self.subTest(ml = ml):
                pc = sy.PWcomplexity(min_len = ml)
                min_l = pc._min_len
                self.assertEqual(min_l, ml)

        # have to override default character class values to test ml < 4
        # NB zero is legal, if potentially stupid
        base_exp={'u':0,'l':0,'d':0,'s':0,'permute':0}
        pc = sy.PWcomplexity(min_len = 0, char_req=base_exp)
        min_l = pc._min_len
        self.assertEqual(min_l, 0)
                
    def test_illegal_values_for_minimum_length(self):
        for ml in (-10, -1, (1+sy.PWdefaults.MAX_LEN) ):
            with self.subTest(ml = ml):
                self.assertRaises(ValueError, sy.PWcomplexity, min_len = ml)
       
    def test_illegal_type_for_min_length(self):
        for ml in ('a', "hello", (1,), {'a':1}, None, 0.5):
            with self.subTest(ml = ml):
                self.assertRaises(TypeError,sy.PWcomplexity, min_len=ml)
        
    def test_illegal_type_for_char_req(self):
        for cr in ('a', "hello", (1,), None, 0.5):
            with self.subTest(cr= cr):
                self.assertRaises(TypeError,sy.PWcomplexity, char_req=cr)
        
    def test_basic_character_class_setting(self):
        for chr_cls in ('u', 'l', 'd', 's', 'permute'):
            expected = {'u':1,'l':1,'d':1,'s':1,'permute':3}
            with self.subTest(chr_cls = chr_cls):
                expected[chr_cls] = 0
                pc = sy.PWcomplexity(char_req=expected)
                got = pc._char_req
                self.assertEqual(expected, got)

    def test_missing_character_class_entry_char_req(self):
        for chr_cls in ('u', 'l', 'd', 's'):
            with self.subTest(chr_cls = chr_cls):
                # make sure we don't break test by accident
                assert(sy.PWdefaults.MIN_CHAR[chr_cls] != 2)
                base_exp = {'u':2, 'l':2, 'd':2, 's':2, 'permute':3}
            # remember to do a shallow copy here
                expected = base_exp.copy()
                del base_exp[chr_cls]
                expected[chr_cls] = sy.PWdefaults.MIN_CHAR[chr_cls]
                pc = sy.PWcomplexity(char_req=base_exp)
                got = pc._char_req
                self.assertEqual(expected, got)   

    def test_positive_partial_entry_in_char_req(self):
        build_exp ={}
        count = 0
        for chr_cls in ('u', 'l', 'd', 's'):
            with self.subTest(chr_cls = chr_cls):
                pc = sy.PWcomplexity(char_req=build_exp)
                got = pc._char_req
                n_perm = pc._char_req['permute']
                if (count > 0):
                    expected = count
                else:
                    expected = sy.PWdefaults.PERMUTE

            #extending build_exp here means we never reach
            # a fully specified class string with missing permute,
            # which is tested in another function below, but also protects
            # test against isolated failure
                build_exp[chr_cls] = 1
                count += 1

                self.assertEqual(expected, n_perm)
                
    def test_zero_value_partial_entry_in_char_req(self):
        build_exp ={}
        count = 0
        for chr_cls in ('u', 'l', 'd', 's'):
            with self.subTest(chr_cls = chr_cls):
                
                pc = sy.PWcomplexity(char_req=build_exp)
                got = pc._char_req
                n_perm = pc._char_req['permute']
                if (count > 0):
                    expected = count
                else:
                    expected = sy.PWdefaults.PERMUTE
                    
               # print (count, chr_cls, build_exp,"Expect: ",
               #        expected," Got: ", n_perm)

            # extending build_exp here means we never reach
            # a fully specified class string with missing permute,
            # which is tested in another function below, but also protects
            # test against isolated failure

                build_exp[chr_cls] = 0
                count += 1
                
                self.assertEqual(expected, n_perm)

                
    def test_isolated_missing_permute_entry(self):
        # make sure we don't break test by accident if defaults change
        # this could generate a secondary failure if total of defaults
        # is close to MIN_LEN
        base_exp = {}
        expected = {}
        for chr_cls in ('u', 'l', 'd', 's'):
            base_exp[chr_cls] = sy.PWdefaults.MIN_CHAR[chr_cls]+1
            expected[chr_cls] = base_exp[chr_cls]
                
        expected['permute'] = sy.PWdefaults.PERMUTE
        pc = sy.PWcomplexity(char_req=base_exp)
        got = pc._char_req
        self.assertEqual(expected, got)
        
    def test_illegal_character_class_value(self):
        for chr_cls in ('u', 'l', 'd', 's'):
            for v in (-1, (1+sy.PWdefaults.MIN_LEN), 1000):
                base_exp = {'u':1, 'l':1, 'd':1, 's':1, 'permute':3}
                with self.subTest(chr_cls = chr_cls):
                    base_exp[chr_cls] = v
                    self.assertRaises(ValueError,
                                      sy.PWcomplexity,char_req=base_exp)

    def test_illegal_type_for_character_class_value(self):
        for chr_cls in ('u', 'l', 'd', 's'):
            for v in ('a', "hello", (1,), {'a':1}, None, 0.5):
                base_exp = {'u':1, 'l':1, 'd':1, 's':1, 'permute':3}
                with self.subTest(chr_cls = chr_cls):
                    base_exp[chr_cls] = v
                    self.assertRaises(TypeError,
                                      sy.PWcomplexity,char_req=base_exp)
  
    def test_illegal_total_for_character_class_values(self):                   
        for chr_cls in ('u', 'l', 'd', 's'):
            base_exp = {'u':1,'l':1,'d':1,'s':1,'permute':3}
            with self.subTest(chr_cls = chr_cls):
                for v in ( -1, 0, 1, 2):
                    base_exp[chr_cls] = (sy.PWdefaults.MIN_LEN - v)
                    self.assertRaises(ValueError,
                                      sy.PWcomplexity,char_req=base_exp)

    def test_illegal_value_for_permute(self):                   
        for v in ( -9, -1, 5, 10):
            base_exp = {'u':1, 'l':1, 'd':1, 's':1, 'permute':3}
            with self.subTest(v = v):
                base_exp['permute'] = v
                self.assertRaises(ValueError,
                                      sy.PWcomplexity,char_req=base_exp)

    def test_illegal_type_for_permute_value(self):
        for v in ('a', "hello", (1,), {'a':1}, None, 0.5):
            base_exp = {'u':1, 'l':1, 'd':1, 's':1, 'permute':3}
            with self.subTest(v = v):
                base_exp['permute'] = v
                self.assertRaises(TypeError,
                                      sy.PWcomplexity,char_req=base_exp)
                
    def test__valid_combined_settings(self):
        # only doing a couple so haven't bothered with wrapping
        # test dicts in array etc
        base_exp={'u':0,'s':2}
        expected={'u':0,'s':2,'l':sy.PWdefaults.MIN_CHAR['l'],
                  'd':sy.PWdefaults.MIN_CHAR['d'],
                  'permute':len(base_exp) }
        pc = sy.PWcomplexity(char_req=base_exp)
        min_l = pc._min_len
        ch_req = pc._char_req
        
        self.assertEqual(min_l, sy.PWdefaults.MIN_LEN)
        self.assertEqual(ch_req, expected)

        base_exp={'l':1,'d':3}
        expected={'l':1,'d':3,'u':sy.PWdefaults.MIN_CHAR['u'],
                  's':sy.PWdefaults.MIN_CHAR['s'],
                  'permute':len(base_exp) }
        pc = sy.PWcomplexity(char_req=base_exp)
        min_l = pc._min_len
        ch_req = pc._char_req
        
        self.assertEqual(min_l, sy.PWdefaults.MIN_LEN)
        self.assertEqual(ch_req, expected)        
                
####################################
### END OF CONSTRUCTOR RELATED TESTS
####################################
        
    def test_illegal_type_password_supplied_to_isComplexityOK(self):
        # have to allow minimum length and zero class size for test
        # to run properly past constructor
        pw=sy.PWcomplexity(char_req = {'u':0,'l':0,'d':0,'s':0,'permute':0},
                            min_len = 0 )
            
        # NB None is considered zero length password and tested below
        for clr_pw in ( 1, (1,), ("a",), {'a':1}, 0.5):
            with self.subTest(clr_pw = clr_pw):
                self.assertRaises(TypeError, pw.isComplexityOK, clr_pw)
                
    def test_isComplexityOK_detects_short_password(self):
        ml = 10
        pw=sy.PWcomplexity(min_len=ml)
        clr_pw = None
        self.assertEqual(pw.isComplexityOK(clr_pw), sy.PWstate.COMPLEX_FAIL)
        self.assertRegex(pw._fail_reason,"Pass phrase length \(\d+\) below")
        clr_pw = ""
        
        for count in range(0, ml):   
            pw=sy.PWcomplexity(min_len=ml)
            with self.subTest(clr_pw=clr_pw):
                self.assertEqual(pw.isComplexityOK(clr_pw), sy.PWstate.COMPLEX_FAIL)
                self.assertRegex(pw._fail_reason,"Pass phrase length \(\d+\) below")
            clr_pw = "".join( (clr_pw,"a") )
            
    def test_isComplexityOK_detects_long_password(self):
        pw=sy.PWcomplexity()
        clr_pw ='a'
        for i in range(0, sy.PWdefaults.MAX_LEN):
            # we already have 1 char in clr_pw and starting iteration from 0,
            # so this just goes over constraint
            clr_pw = "".join( (clr_pw, "a") )
            
        self.assertEqual(pw.isComplexityOK(clr_pw), sy.PWstate.COMPLEX_FAIL)
        self.assertRegex(pw._fail_reason,"Pass phrase length \(\d+\) exceeds")

    def test_isComplexityOK_detects_individual_inadequate_char_class(self):
        base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
        test_pw = {'u':"password1!", 'l':"PASSWORD1!", 'd':"Password!*", 's':"Password12"}
        
        for chr_cls in ('u', 'l', 'd', 's'):
            with self.subTest(chr_cls = chr_cls):
                base_exp[chr_cls]=1
                pw = sy.PWcomplexity(char_req=base_exp)
                self.assertEqual(pw.isComplexityOK(test_pw[chr_cls]), sy.PWstate.COMPLEX_FAIL)
                self.assertRegex(pw._fail_reason,"Pass phrase contains only")
                base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
    
    def test_isComplexityOK_accepts_individual_adequate_char_class(self):
        base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
        test_pw = {'u':"Password1!", 'l':"pASSWORD1!", 'd':"Password1*", 's':"Password1!"}
        
        for chr_cls in ('u', 'l', 'd', 's'):
            with self.subTest(chr_cls = chr_cls):
                base_exp[chr_cls]=1
                pw = sy.PWcomplexity(char_req=base_exp)
                self.assertEqual(pw.isComplexityOK(test_pw[chr_cls]), sy.PWstate.COMPLEX_OK)
                base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
    
    def test_isComplexityOK_detects_inadequate_permutation(self):
        base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
        test_pw = {'2':"PASSWORDDD", '3':"pASSWORDDD", '4':"pASSWORD11"}
        
        for n_permute in (2, 3, 4):
            with self.subTest(n_permute = n_permute):
                base_exp['permute']=n_permute
                pw = sy.PWcomplexity(char_req=base_exp)
                self.assertEqual(pw.isComplexityOK(test_pw[str(n_permute)]), sy.PWstate.COMPLEX_FAIL)
                self.assertRegex(pw._fail_reason,"Pass phrase needs to contain")
                base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
    
    def test_isComplexityOK_accepts_adequate_permutation(self):
        base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
        test_pw = {'1':"PASSWORDDD", '2':"Passworddd", '3':"Passwordd1", '4':"Password1!"}
        
        n_permute = 1
        for chr_cls in ('u', 'l', 'd', 's'):
            with self.subTest(chr_cls = chr_cls):
                base_exp[chr_cls]=1
                base_exp['permute'] = n_permute
                pw = sy.PWcomplexity(char_req=base_exp)
                self.assertEqual(pw.isComplexityOK(test_pw[str(n_permute)]), sy.PWstate.COMPLEX_OK)
                n_permute += 1
                
    def test_repr_returns_correct_values(self):
        base_exp = {'u':0, 'l':0, 'd':0, 's':0, 'permute':1}
        ml = 10
        
        pw = sy.PWcomplexity(char_req=base_exp, min_len=ml)
        repr_str = repr(pw)
        match_str="".join( ("Required minimum pass phrase length: ", str(ml)))
        self.assertRegex(repr_str, match_str)
        match_str="".join( ("Maximum allowed pass phrase length: ", str(sy.PWdefaults.MAX_LEN)))
        self.assertRegex(repr_str, match_str)
        match_str="".join( ("Require ", str(base_exp['permute']), " out of following character classes") )
        self.assertRegex(repr_str, match_str)
        for chr_cls in ('u', 'l', 'd', 's'):
            match_str="".join( (" need minimum of ", str(base_exp[chr_cls])," characters"))
            self.assertRegex(repr_str, match_str)
                
    def tearDown(self):
        pass

###########################
### End of TestPWcomplexity        
###########################        
        

class TestPathwell(unittest.TestCase):
    def test_default_contructor(self):
        pw = sy.Pathwell()
        
    @unittest.skip("focusing on doing complexity properly")
    def test_parse_pattern_blacklist_file(self):
        pw = sy.userPW()
        rl = pw.parse_pattern_blacklist_file("../config/pw_topology_blacklist.txt")
        #for r in rl:
        #    print (r)
        #assert(len(rl)==100)    # for the top 100 list
        
    @unittest.skip("focusing on doing complexity properly")
    def test_pattern_tests(self):
        pw = sy.userPW()
        rl = pw.parse_pattern_blacklist_file("../config/pw_topology_blacklist.txt")
        pt = pw.pattern_tests("Password1",rl)
        pt = pw.pattern_tests("Password1!",rl)

        pw.convert_ulds_template_to_hint("ulllllllds")
        h=pw.convert_ulds_template_to_hint("ullllllldsdsul",
                                         extchar={'u':'I','l':'i','d':'9','s':'#'})
        print (h)
        h=pw.convert_ulds_template_to_hint("ullllllldsdsul",basehint="Summer16",
                                         extchar={'u':'I','l':'i','d':'9','s':'#'})
        print (h)
        ## tests straight assert, look at skip test options
        #h=pw.convert_ulds_template_to_hint("ullllllldsdsul",basehint="Summer16",
        #                                 extchar={'u':'I','l':'i','d':'9'})
        #print (h)
        t=  pw.convert_hint_to_ulds_template("Password1!")
        print(t)


##########################
        
class TestLockout(unittest.TestCase):    
    @classmethod
    def setUpClass(cls):
        pass
        
    def setUp(self):
        pass
        
    def test_default_constructor(self):
        lck = sy.Lockout()
        dly = lck._delay_dict
        self.assertEqual(dly,{0:0})
		#lck = sy.Lockout(None)
		#dly = lck._delay_dict
        #self.assertEqual(dly,{0:0})
		
    def test_rejects_invalid_delay_dict(self):
        test_vars = (1, 1.5, 'a', 'hello', (0,1))
        for t in test_vars:
            with self.subTest(t = t):
                self.assertRaises(TypeError,sy.Lockout,t)
	
    def test_rejects_invalid_types_for_attempts_in_dict(self):
        test_vars = ( {'a':0}, {'hello':0}, {(0,1):0} )
            # floats are duck typed, but an warning is issued to the log
        for t in test_vars:
            with self.subTest(t = t):
                self.assertRaises(TypeError,sy.Lockout,t)
	
    @unittest.skip("defer while think about proper return value from function")
    def test_rejects_invalid_types_for_response_in_dict(self):
        test_vars = ( {0:1.5}, {-1:10}, {0:'a'}, {0:'hello'}, 
                    {0:(0,1)}, {1.5:0}, {'a':0}, {'hello':0}, {(0,1):0} )
        for t in test_vars:
            with self.subTest(t = t):
                self.assertRaises(TypeError,sy.Lockout,t)
	
    def test_rejects_invalid_n_attempts_in_dict(self):
        for n_attempts in range(-10, 0):
            with self.subTest(n_attempts = n_attempts):
                self.assertRaises(ValueError,sy.Lockout,{n_attempts:0})
                
    def test_lockout_rejects_wrong_type_in_fetch_lockout(self):
        test_vars = ('a', 'hello', (0,1), {0:1} ) # skip floats as duck type
        lck = sy.Lockout()
        for t in test_vars:
            with self.subTest(t = t):
                self.assertRaises(TypeError, lck.fetch_lockout,t)
                
    def test_lockout_rejects_invalid_n_attempts_in_fetch_lockout(self):
        lck = sy.Lockout()
        for n_attempts in range(-10, 0):
            with self.subTest(n_attempts = n_attempts):
                self.assertRaises(ValueError,lck.fetch_lockout,n_attempts)
	    
## will need to change if decide to change return type on fetch_lockout
    def test_fetch_lockout_returns_correct_delay(self):
        delays = { 0:0, 5:10, 10:120 }
        lck = sy.Lockout(delay_dict=delays)
        for (n_attempts, dly) in sorted(delays.items(), reverse=True):
            
            with self.subTest(n_attempts = n_attempts):
                self.assertEqual(lck.fetch_lockout(n_attempts), dly)
                self.assertEqual(lck.fetch_lockout(n_attempts+1), dly)
            
    

###########################
### End of TestPWcomplexity        
###########################        
        
            
if __name__ == '__main__':
    unittest.main()
    
    
