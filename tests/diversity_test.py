#
# diversity module tests
# 12 Apr 2016, Charles Bird
#

# remember the test_ prefix is mandatory

import unittest
import sys
import os

from diversity import diversity as dv

class TestDiversityIndex(unittest.TestCase):    
    @classmethod
    def setUpClass(cls):
        pass
        
    def setUp(self):
        pass


    def test_default_contructor(self):
        # q is the order parameter
        for q in range(-10,11):
            with self.subTest(q = q):
                dq = dv.Diversity(q)
                set_q = dq._order    
                self.assertEqual(set_q, q)
        
                
####################################
### END OF CONSTRUCTOR RELATED TESTS
####################################
        
                
    def tearDown(self):
        pass

###########################
### End of TestDiversityIndex        
###########################        

        
            
if __name__ == '__main__':
    unittest.main()
    
    
