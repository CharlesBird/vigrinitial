var Vigr = (function () {
  var privateMethod = function () {
    // do something
  };

  var someMethod = function () {
    // public
  };

  var anotherMethod = function () {
    // public
  };
  
  return {
    someMethod: someMethod,
    anotherMethod: anotherMethod
  };
  
})();

var VigrExt = (function (Vigr) {
    
    Vigr.extension = function () {
        // another method!
    };
    
    return Module;
    
})(Vigr || {console.log("Warning - unable load Vigr base module");});