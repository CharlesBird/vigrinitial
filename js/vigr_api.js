//
// vigr API management module
//
// (c) Charles Bird, 2016
//

// create an API "class"


var ApiResource = function (api_host = "localhost",
                            api_port=443, 
                            api_resource="/",
                            description){
    this.dscrptn    = ""
    this.protocol   = "https"  // don't allow downgrading to http in constructor
                                // for security reasons, it should be set by an explicit function
    this.host       = api_host  // gotcha on DNS lookups here
    this.port       = api_port
    this.res_name   = api_resource
    // more stuff needed here on methods and authentication
}

// this should be done on a per resource, not per server, basis
// eg a version-returning API using GET may well be http   
ApiResource.prototype.allow_http(){this.protocol="http";};

var VigrAPI = (function (Vigr)  {
    
    Vigr.registerAPI = function (apiObj) {
        api_resources.push(apiObj);
    };
    
    Vigr.listRegisteredAPIs = function(){
        api_list();
        for api in api_resources{
                console.log(api);
        }
        return (api_list);
    };
    
    return Vigr;
    
})(Vigr || {console.log("Warning - unable load Vigr base module");});
