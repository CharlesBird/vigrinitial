var dataset = [
                  [ 0, 5,     20 ],
                  [ 1, 480,   90 ],
                  [ 2, 250,   50 ],
                  [ 3, 100,   33 ],
                  [ 4, 330,   95 ],
                  [ 5, 410,   12 ],
                  [ 6, 475,   44 ],
                  [ 7, 25,    67 ],
                  [ 8, 85,    21 ],
                  [ 9, 220,   88 ]
              ];
			  
var dataset2 = [
                  [ 0, 5,     120 ],
                  [ 1, 480,   290 ],
                  [ 2, 250,   150 ],
                  [ 3, 100,   333 ],
                  [ 4, 330,   295 ],
                  [ 5, 410,   212 ],
                  [ 6, 475,   44 ],
                  [ 7, 25,    167 ],
                  [ 8, 85,    55 ],
                  [ 9, 220,   288 ],
				  [ 10, 144, 3]
              ];

//Create SVG element	- pick these up from document idc
// much easier if use jQuery and gridster

var w = 400;	
var h = 400;
var padding = 20;

var num_charts = 6;
//var chart_w = (w - icon_size[0] - 2* icon_pad[0])/2;
//var chart_h = (h - 2 * icon_pad[1])/4;
var chart_w = w /2;
var chart_h = h /2;


// these scale within the subareas
var xScale = d3.scale.linear()
            //.domain([0, d3.max(dataset, function(d) { return d[1]; })])
            .domain([0, 800])
            .range([ padding, chart_w - ( padding *2 ) ]);
//.range([ padding, w - ( padding *2 ) ]);

var yScale = d3.scale.linear()
			//.domain([0, d3.max(dataset, function(d) { return d[2]; })])
			.domain([0, 500])
            .range([chart_h - padding, padding]);	// flip origin
			 
var rScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d[2]; })])
            .range([1, 30]);
					 
		 
var xAxis = d3.svg.axis()
				.scale(xScale)
				.orient("bottom")
				.ticks(5);						 
		 
var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.ticks(5);	
			
	  
// listeners for drag and drop
var dragged_icon;
var start_x;
var start_y;

var iconDrag = d3.behavior.drag()
    .on("dragstart", function(){
        // create a ghost icon to drag
		dragged_icon = d3.select(this);
					
		start_x = dragged_icon.attr("x");
		start_y = dragged_icon.attr("y");

		// need an on.entry type event to trigger drawing of chart ghost
		
		var ghost = d3.select(".dropsites").selectAll(".ghostIcon")	// local or obj??
			.data(d3.range(1).map(function(){return 1;}))
			.enter()
			.append("circle")
			.attr("class","ghostIcon")
			//.attr("width", icon_size[0])
			//.attr("height", icon_size[1])
			.attr("radius",5)
			.attr("cx", d3.event.x)
			.attr("cy", d3.event.y)	// need to pass over selection event here
			.call(ghostDrag);
			
			// not sure if rect on rect is allowed
			// need to copy over data from selected icon
			// but then act on the ghost icon alone
		
    })
    .on("drag", function(){
        //hey we're dragging, let's update some stuff
		//.on("drag", drag_ghost )){}
		
		d3.select(this)
			.attr("fill","blue;")
			.attr("x", d3.event.x)
			.attr("y", d3.event.y);
    })
    .on("dragend", function(){
        //we're done, end some stuff
		// need to update chart list with chart type and location
		// instantiate chart with data
		// and remove the ghost icon
		// either use voronoi or scale invert for drop determination
		dragged_icon.transition()
			.attr("x",start_x)
			.attr("y",start_y)
		
    });
			
// drag a semitransparent marker onto chart area grid
function ghostDrag (d){
	d3.select(this)
			.attr("fill","blue;")
			.attr("x", d3.event.x)
			.attr("y", d3.event.y);
}	

// here we should be operating on this as background
// we should maintain a stack of svg groups that can
// used for enter/update/exit and listening events
	
// using two divs pushes to right	

// rationalise height/width and position tracking idc
//var chart_area = d3.select(".dropsites")
            //.append("svg")
			//.attr("class","chartArea")
			//.attr("width", w)
            //.attr("height", h)
			//.attr("style", "outline: 1px rgb(50,50,50);");
			
//// track icon bar position in dynamic example
	//.attr("width",function(d){return w - icon_size[0] - 2* icon_pad[0]})
	//.attr("height",function(d){return h - 2 * icon_pad[1]})
	

// should relate these to div widths in css
var icon_size = [ 75, 75 ];		// x, y
var icon_pad = [ 10, 10];
var icon_bar = [ w-icon_size[0]-icon_pad[0], icon_pad[1], 0, 85];	// xStart, yStart, xStep, yStep
// will need onload dynamic adjust
		

// create this as a flush left div??
// selecting svg only displays one, body or div shows all
var i =0;
var chartGrid = d3.select(".dropsites").selectAll("svg")
//var chartGrid = chart_area.select("chart_area")
    .data(d3.range(num_charts).map(function() { return {x: chart_w / 2, y: chart_h / 2}; }))
    .enter()
	.append("svg")
	.attr("id",function(d,i){return "graph"+i++;})
	//.append("g")		// we need to group for transform operations, but forces into single columns
    .attr("width", chart_w)
    .attr("height", chart_h)
	.attr("style", "border: 1px rgb(0,100,100);");

// mark drop targets	
chartGrid.append("circle")
	.attr("class", "dropMarker")
	.attr("r", "5px")
	.attr("cx", function(d) { return d.x + icon_size[0]/2;})
	.attr("cy", function(d) { return d.y + icon_size[1]/2;});
		
			
// need to redesign to cope with div
			
var icon_options = [
	["Scatter","test.png"],
	["Line","test.png"],
	["Phylogram","test.png"]
];


var i = 0;	// count of number icons

var svg_icons = d3.select(".iconbar")
	.append("svg")
	.attr("width",100)
	.attr("height",600)
	.attr("fill","rgb(130,230,230);");

var chart_icons = svg_icons.selectAll("rect")
	.data(icon_options)
    .enter()
    .append("rect")
	.attr("class", "icon")
	.attr("icon_id",function(d,i){return "icon"+ i;})
	.attr("width",icon_size[0])
    .attr("height",icon_size[1])
	.attr("x",icon_pad[0])
	.attr("y",function(d,i){ return icon_pad[1]+(icon_bar[3]*i++); })
	//.attr("x",function(d){return icon_bar[0] + (icon_bar[2]*i) })
	//.attr("y",function(d){return icon_bar[1] + (icon_bar[3]*i++) })
	.call(iconDrag)
	//.style("fill","url(#icon)")
// need to add function to wrap around to extra row
	//.text(function(d){return d[0];})
	;
	
	
// add some demo charts for brushing

//var chart_svg = chartGrid[0];
//chart_svg = d3.selectAll(".dropsites svg");

// debug
/*
chart_svg.append("line")
	.attr("x1","0px")
	.attr("y1","0px")
	.attr("x2","50px")
	.attr("y2","50px")
	.attr("stroke","black");
	
d3.select(".dropsites #graph1").append("line")
	.attr("x1","0px")
	.attr("y1","50px")
	.attr("x2","50px")
	.attr("y2","0px")
	.attr("stroke","black");
*/

// ultimately supply graphID from drag/drop operation
d3.select(".dropsites #graph1 circle").remove();
d3.select(".dropsites #graph2 circle").remove();

chart_svg = d3.select(".dropsites #graph1");
chart_svg.attr("style","outline: 2px solid gray;");



// works, but takes fill from background class - need to reset class to change
/*
chart_svg.append("circle").attr("r",20)
	.attr("cx",20).attr("cy",20).attr("background-color","green;").attr("style","outline: 2px solid black;")
	.attr("class","dropMarker");
*/
	
// put a hidden tooltip div
// don't attach this to the body element here as it then appears below the graphs
var tooltipDiv = d3.select(".dropsites").append("div")   
    .attr("class", "tooltip")               
    .style("opacity", 0.50); // show this for debugging
    
    
var new_chart = chart_svg.append("svg").append("g").attr("width",chart_w).attr("height",chart_h)
			.attr("class", "chartArea")
			.attr("style", "outline: 1px solid red;");

// populate graph with array data
new_chart.selectAll("circle")
	.data(dataset)
	.enter()
	.append("circle")
	.attr("id", function(d,i){
		return "point"+d[0];	// bad name, but do for now
	})
	.attr("cx", function(d) {
       return xScale(d[1]);
	})
	.attr("cy", function(d) {
        return yScale(d[2]);
	})
	.attr("r", function(d) {
		return rScale ((d[2]/(2+d[1]) ) );	// div by 0 risk
                                                // scale it down a bit for brush demo
	})
    .on("mouseover", function(d) {              // tooltip demo
            tooltipDiv.transition()             // this will have to have lookthrough to work with brushing
                .style("opacity", .9);      
                
            tooltipDiv.html("Point: "+d[0]+ "= ( "+d[1]+","+d[2]+") <br/>"  + d.close)  
                .style("left", (d3.event.pageX) + "px")     
                .style("top", (d3.event.pageY - 28) + "px"); 
            console.log("In tooltip mouseover");
    })                  
    .on("mouseout", function(d) {       
            tooltipDiv.transition()        
                .duration(200)      
                .style("opacity", 0);   
    });
        
        
new_chart.selectAll("text")
   .data(dataset)
   .enter()
   .append("text")
   .text(function(d) {
        return d[1] + "," + d[2];
   })
   .attr("x", function(d) {
        return xScale(d[1]);
   })
   .attr("y", function(d) {
        return yScale(d[2]);
   })
   .attr("font-family", "sans-serif")
   .attr("font-size", "11px")
   .attr("fill", "red");
   

///// add second chart with different data for brushing demo


chart_svg = d3.select(".dropsites #graph2");
chart_svg.attr("style","outline: 2px solid green;");

var new_chart = chart_svg.append("svg").append("g").attr("width",chart_w).attr("height",chart_h)
			.attr("class", "chartArea");

			
			
// populate graph with array data
new_chart.selectAll("circle")
	.data(dataset2)
	.enter()
	.append("circle")
	.attr("id", function(d,i){
		return "point"+d[0];	// bad name, but do for now
	})
	.attr("cx", function(d) {
       return xScale(d[1]);
	})
	.attr("cy", function(d) {
        return yScale(d[2]);
	})
	.attr("r", function(d) {
		return rScale ((d[2]/(2+d[0])/3) );	// div by 0 risk and scale down for demo
		});


new_chart.selectAll("text")
   .data(dataset2)
   .enter()
   .append("text")
   .text(function(d) {
        return d[1] + "," + d[2];
   })
   .attr("x", function(d) {
        return xScale(d[1]);
   })
   .attr("y", function(d) {
        return yScale(d[2]);
   })
   .attr("font-family", "sans-serif")
   .attr("font-size", "11px")
   .attr("fill", "red");

// json driven
chart_svg = d3.select(".dropsites #graph3");
chart_svg.attr("style","outline: 2px solid green;");

var data_id = ["sample1", "sample2"];
var colors = ["blue","red"];
var cnt = 2;


// build a chart handle - link this to naming in flexbox

// working separate chart version
//var json_chart = chart_svg.append("svg").append("g").attr("width",chart_w).attr("height",chart_h)
//				.attr("class", "chartArea");
var json_chart = chart_svg.append("#graphDrop").append("g").attr("width",chart_w).attr("height",chart_h)
				.attr("class", "chartArea");

for (u in data_id){
		
	console.log(data_id[u]);
	var selector = "#"+data_id[u];
		
	var d3_url= build_api_url("http","localhost:8000","xy",data_id[u]);
		
	// populate graph with array data
	console.log(d3_url);
			
	console.log(selector);

	plot_graph(json_chart, selector, d3_url, cnt);
				
	cnt = cnt + 5;
}
   
function plot_graph(chart_handle, selector_id, data_url, rad) {
 	d3.json(data_url, function(error, json){	// this is async, so need to freeze data_id in call **
			if (error) throw error;

			chart_handle.selectAll(selector_id)
				.data(json)
				.enter()
				.append("circle")
				.attr("cx", function(d) {
				   return xScale(d[0]);
				})
				.attr("cy", function(d) {
					return yScale(d[1]);
				})
				.attr("id", selector_id)
				.attr("r", rad);  
	}); 
}	
   
   
// data collection functions   
   
   
// name collision possible here
// with multiple data sources
var xy_values = new Object();	// investigate closures to avoid global
								// will need have an isReady added for async usage

async_store_json_from_api("http","localhost:8000","xy","sample1",xy_values);
async_store_json_from_api("http","localhost:8000","xy","sample2",xy_values);

console.log(xy_values);
setTimeout(function(){console.log(xy_values);}, 500 );

/// set up data access functions   
////////////////////////////////

function build_api_url(prtcl, api_base_url, srvc, data_id){
	var api_url = prtcl+"://"+api_base_url+"/"+srvc+"/"+data_id;
	return (api_url);
}
	
// need to think about how to manage visualisation here
// doing the data storage approach will mandate prefetch
// rather than render during json fetch	
	
function async_store_json_from_api(prtcl, api_base_url, srvc, data_id, store_obj){
	// this is an asynchronous load from url (essentially prefetch)
    // should really integrate with some cache functionality
	// check the isReady value on store_obj before using
	var api_url = build_api_url(prtcl, api_base_url, srvc, data_id);
	
	d3.json(api_url, function(error, json){
		if (error) throw error;
		// should, and where, a check on overwrite be enforced?
		store_obj[data_id] = json;		// this only works on internals
										// because of js approach of pass-by-value
    }); 
}
