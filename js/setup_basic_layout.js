//
// currently the "main function"
// but could probably inline idc, once fully modularised

// this pickups the config objects from the local_data.js file now
// they should be wrapped idc

// this is "main" - in the DOM Ready pattern
// jquery dependency missing - need to add system configuration checks

// probably need to do some module / public interface stuff as well
// pass in the jQuery global, but still doesn't seem happy

//$(function ($) {
(function ($) {

    
    var mode = "run";       
    // var mode = "debug";     // install some test data and additional logging
    
    // browser dependency checks
    // Check for the various File API support.
    
    // redundant now - server handles data files
    /*
    if (window.File && window.FileReader && window.FileList && window.Blob) {
    } else {
        window.alert('The File APIs are not fully supported in this browser.');
    }
    */
    
    
    // local storage stuff
    if (typeof(Storage) == "undefined") {
        Vigr.setBrowserCap("local_storage","")      // js treats "" as boolean false
        window.alert("Unable to initialise local storage.\nNo local libraries will be available");        
		console.log("Unable to initialise local storage.\nNo local libraries will be available");
    }else{
        Vigr.setBrowserCap("local_storage","TRUE")
    }
    
    
    Vigr.init( true);
  
    // debug: upload mock data
    if ( ("debug" == mode) && (false == Boolean(localStorage.getItem("vigr_mock")) ) ){
        console.log("No vigr data in local store - loading mock data");
    //    window.alert("No vigr data in local store - loading mock data");
        Vigr.saveLibrariesToLocalStore(mock_libraries);
        Vigr.saveDatasetsToLocalStore(mock_datasets);
        localStorage.setItem("vigr_mock","true");
        // ?? trigger rebuild events
        Vigr.fetchLibraries();
        Vigr.fetchDatasets();
    
    }
    
    
    Vigr.buildGraphArea( graphAreaOptions );
    Vigr.buildGraphToolbar("#graphIconBar", graphIcons);

    // enable these jQuery UI functions at DOM ready stage
    $(function() {
    //$(document).ready(function() {    
    
    // change to class, pickup css selectors from cssname object & standardise options deployment idc
    
		$( "#toolbar_accordion" ).accordion({
            heightStyle: "content",
            event: "click hoverintent"
        });
		
        $(document).tooltip();
        
        $(".resizable").resizable();
        $(".sortable").sortable( {  handle: ".graphMenu" });
    });
    
    // various test/debug event fixtures
    // ===========================
	//
    // $(document).on("click", ".cancel_btn", function(){$(this).trigger("testEvent")});
	// $(document).on("confirmTool", "", Vigr.handleTestEvent);    // testing    
    $(document).on("testEvent", "" , Vigr.handleTestEvent);      
    //$(document).on("closedGraph", "" , Vigr.handleTestEvent);      
    
      
    // "button" event handlers convert clicks to semantic events so 
    // shared interaction/programmatic handlers can be accessed
    // TODO this could implemented in a map
    // ============================================================

    // -- need to sort out the "div" click versions
    
    $(document).on("click", ".cancel_btn", function(){$(this).trigger("explicitCancelTool")});
    $(document).on("click", ".confirm_btn", function(){$(this).trigger("confirmTool")});
    
    $(document).on("click", ".closeicon", function(){$(this).trigger("evtCloseGraph")});

   
    // semantic event handlers
    // =======================

    // "cancel" events
    //
    // this runs for _all_ explicitCancelTool events to hide any sliding panel
    // including executing markPanelClean if an isDirty attribute is present on panel
    $(document).on("explicitCancelTool", "", Vigr.hidePanelHandler);
	//
    // add any specific data discards etc below, tagged to vigr_fn_ class
    $(document).on("explicitCancelTool", ".vigr_fn_load_lib", function(){$(this).trigger("updateLibList")} );
	
	// BUG IN toggle behaviour for register src
	$(document).on("explictCancelTool", ".vigr_fn_register_src", function(){$("form.vigr_fn_register_src")[0].reset();});
    $(document).on("confirmTool", ".vigr_fn_register_src", Vigr.confirmRegisterDataSrcHandler);
    
    // confirmation events
    //
    $(document).on("confirmTool", ".vigr_fn_load_lib", Vigr.confirmLibSelectionHandler );
    $(document).on("confirmTool", ".vigr_fn_clear_lib", Vigr.clearSelectionsHandler);
    $(document).on("confirmTool", ".vigr_fn_add_local_file", Vigr.addLocalFileHandler);
    
    
    // graph events
    // these only relate to display not underlying data
    //
    $(document).on("evtCloseGraph", "", Vigr.closeGraphHandler);
    
    // placeholders
    //$(document).on("graphClosed", "", Vigr.handleTestEvent);
    
    
    // resize events have to trigger graph redraws - use generic updateGraphs event
    $(document).on("evtUpdateGraphs","",Vigr.updateGraphHandler); 
        
    //$(document).on("hidePanel", "", Vigr.handleTestEvent);  // checking if these are being generated still
    //$(document).on("hidePanel", "", Vigr.hidePanelHandler);

    // internally generated
    //
    $(document).on("toggleToolOff", "" , Vigr.toggleToolOffHandler);
    $(document).on("updateLibList", "", Vigr.updateLibraryListHandler);
    
    // brushes
    //$(document).on("clearDynamicBrushes", "svg", Vigr.clearAllDynamicBrushHandler);
    $(document).on("propagateBrush", "svg", Vigr.brushAllGraphsHandler);
    

    // data interactions (loads, API calls etc)
    //
    
    // remember to use async stuff on background data loads **
        
    // local files handler (in native js style)
    document.getElementById('local_files').addEventListener('change', Vigr.handleFileSelect, false);

    
    // firing test event
    //$.event.trigger("testEvent");
   
    // test code
    ///////////////////////
    
   // OK, there is clear a default function that Vigr should execute to provide
   // module load event
   /*
    if (window.Vigr.VIgR) {
       console.log("Found loaded VIgR module");
    }else{
        console.log("VIgR module not reporting load event");
    }
    */
    
    <!-- test code TODO -->
    
    
    
    // test event and API
    /////////////////////
    var capAPI = new Vigr.apiObj( "http", "localhost", 8000, "capabilities");
    // the wierd syntax gets round the scoping issue
    
//    $(document).on("click", ".vigr_fn_test_api", function(){( capAPI.async_query_api("", capAPI ) ) });
    
    
    // ideally want to use https for all connections, but on local host would use self-signed cert
    // which lots of browsers kick back on
    
    var testPost = new Vigr.apiObj( "http", "localhost", 8000, "testPostForm", enumMethodT.POST);
    var testGet = new Vigr.apiObj( "http", "localhost", 8000, "testGet", enumMethodT.GET);
    var fetchLibData = new Vigr.apiObj( "http", "localhost", 8000, "fetchLibraryData", enumMethodT.GET, 
                                        Vigr.processLibDataMsg);
    
   
    // need look at trailing / behaviour of falcon
    var ping = new Vigr.apiObj( "http", "localhost", 8000, "ping", enumMethodT.GET);
    
    // this will only work as a way of checking for missing server if we access the jqXHR.fail() method or similar
    ping.call( {}, "" );    // need to give empty param string on simple gets
    
    
    var APIdiversityProfile = new Vigr.apiObj( "http", "localhost", 8000, "diversityProfile", enumMethodT.POST,
                                Vigr.echoJson );
    
    
    // one problem with callback approach, makes it difficult to attach multiple handlers unless use wrapper
    var APImorisitaHorn = new Vigr.apiObj( "http", "localhost", 8000, "morisitaHorn", enumMethodT.POST,
                                Vigr.processVigrDataPackage );
                               // Vigr.echoJson );
    //var d=[100,101]
    //var a=[100,101]
    //APImorisitaHorn.attach_arr_data_id(d);
    //APImorisitaHorn.attach_arr_adapter_id(a);
    
    // run this here for testing, but then move to the load(mocks) and add callback for LoadLib/Data
    
    //testGet.call( {} )
    fetchLibData.call( {} )  // part of setup
    
    
    // just testing comms for now
    /*
    APImorisitaHorn.call( { "lib1":"foo", "data1":"example",
                            "lib2":"foo", "data2":"test",
                            "group":"IGH"
                       } )
    */
    
    // testing
    $(document).on("click", ".vigr_fn_test_api", function(){ console.log("calling API");
                                                           //testPost.call({"name":"fred", "species":"rat"});
                                                           //testGet.call({"name":"fred", "species":"rat"});
                                                           APIdiversityProfile.call({
                                                                        order_start:0,
                                                                        order_end: 5
                                                                        },
                                                                        "foo/example")
                                                           });
    
    // populateLoadDataLibPanel("vigr_fn_load_lib");
    
    // testing - will need to have event dispatch table manage updates
    /*
    $(document).on("apiComplete","", function(e){ console.log("data returned in api "+e.type+":"+e.apiID);
                                                 console.log(e);
                                                 console.log( "api data ");
                                                 console.log( _api[e.apiID].rawdata );
                                                });
    */

// don't need this is if there is a callback attached to the API
//    $(document).on("apiComplete","", function(e){ handleCompletedAPI( _api[e.apiID]); } );
    
    
    // example 
    
    //$(document).on("capabilitiesLoaded","",function(){console.log(capAPI.rawdata);})
    
    
// add jQuery, D3, Vigr, gridstack dependencies
})(jQuery);
/*
})( Vigr    || {console.log("Warning - unable load Vigr base module");}, 
    jQuery  || {console.log("Warning - unable load jQuery");}, 
    D3      || {console.log("Warning - unable load D3");},
    gridstack      || {console.log("Warning - unable load gridstack");}
    );
*/


