
//Create SVG element	- pick these up from document idc
// much easier if use jQuery and gridstack


var w = 400;	
var h = 400;
var padding = 20;

var num_charts = 6;
//var chart_w = (w - icon_size[0] - 2* icon_pad[0])/2;
//var chart_h = (h - 2 * icon_pad[1])/4;
/* var chart_w = w /2;
var chart_h = h /2;
 */
 var chart_w = 50;  // lets make sure these fit in the gridstack objects for now
 var chart_h = 50;

// these scale within the subareas
var xScale = d3.scale.linear()
            //.domain([0, d3.max(dataset, function(d) { return d[1]; })])
            .domain([0, 800])
            .range([ padding, chart_w - ( padding *2 ) ]);
//.range([ padding, w - ( padding *2 ) ]);

var yScale = d3.scale.linear()
			//.domain([0, d3.max(dataset, function(d) { return d[2]; })])
			.domain([0, 500])
            .range([chart_h - padding, padding]);	// flip origin
			 
var rScale = d3.scale.linear()
//            .domain([0, d3.max(dataset, function(d) { return d[2]; })])
            .range([1, 30]);
					 
		 
var xAxis = d3.svg.axis()
				.scale(xScale)
				.orient("bottom")
				.ticks(5);						 
		 
var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.ticks(5);	
			
// should relate these to div widths in css
var icon_size = [ 75, 75 ];		// x, y
var icon_pad = [ 10, 10];
var icon_bar = [ w-icon_size[0]-icon_pad[0], icon_pad[1], 0, 85];	// xStart, yStart, xStep, yStep
// will need onload dynamic adjust
		

// this is what's breaking the vigr... version, because we're returning an svg in index.html        
        
// create this as a flush left div??
// selecting svg only displays one, body or div shows all
var i =0;
var chartGrid = d3.select(".dropsites").selectAll("svg")
//var chartGrid = chart_area.select("chart_area")
    .data(d3.range(num_charts).map(function() { return {x: chart_w / 2, y: chart_h / 2}; }))
    .enter()
	.append("svg")
	.attr("id",function(d,i){return "svg_graph"+i++;})
	//.append("g")		// we need to group for transform operations, but forces into single columns
    .attr("width", chart_w)
    .attr("height", chart_h)
	.attr("style", "border: 1px rgb(0,100,100);");

// mark drop targets	
//add_drop_tgt_marker_to_svg_handle(chartGrid);
			
	
//chart_svg = d3.select(".dropsites #graph1");
//chart_svg.attr("style","outline: 2px solid gray;");

    /*
var new_chart = chart_svg.append("svg").append("g").attr("width",chart_w).attr("height",chart_h)
			.attr("class", "chartArea")
			.attr("style", "outline: 1px solid red;");
*/
            /*
// populate graph with array data
new_chart.selectAll("circle")
	.data(dataset)
	.enter()
	.append("circle")
	.attr("id", function(d,i){
		return "point"+d[0];	// bad name, but do for now
	})
	.attr("cx", function(d) {
       return xScale(d[1]);
	})
	.attr("cy", function(d) {
        return yScale(d[2]);
	})
	.attr("r", function(d) {
		return rScale ((d[2]/(2+d[1]) ) );	// div by 0 risk
                                                // scale it down a bit for brush demo
	}) ;
    
    */
   // .on("mouseover", function(d) {              // tooltip demo
   //         tooltipDiv.transition()             // this will have to have lookthrough to work with brushing
   //             .style("opacity", .9);      
   //             
   //         tooltipDiv.html("Point: "+d[0]+ "= ( "+d[1]+","+d[2]+") <br/>"  + d.close)  
   //             .style("left", (d3.event.pageX) + "px")     
   //             .style("top", (d3.event.pageY - 28) + "px"); 
   //         console.log("In tooltip mouseover");
   // })                  
   // .on("mouseout", function(d) {       
   //         tooltipDiv.transition()        
   //             .duration(200)      
   //             .style("opacity", 0);   
   // })
   

/*   
new_chart.selectAll("text")
   .data(dataset)
   .enter()
   .append("text")
   .text(function(d) {
        return d[1] + "," + d[2];
   })
   .attr("x", function(d) {
        return xScale(d[1]);
   })
   .attr("y", function(d) {
        return yScale(d[2]);
   })
   .attr("font-family", "sans-serif")
   .attr("font-size", "11px")
   .attr("fill", "red");
*/

///// add second chart with different data for brushing demo

/*
chart_svg = d3.select(".dropsites #graph2");
chart_svg.attr("style","outline: 2px solid green;");

var new_chart = chart_svg.append("svg").append("g").attr("width",chart_w).attr("height",chart_h)
			.attr("class", "chartArea");
		
// populate graph with array data
new_chart.selectAll("circle")
	.data(dataset2)
	.enter()
	.append("circle")
	.attr("id", function(d,i){
		return "point"+d[0];	// bad name, but do for now
	})
	.attr("cx", function(d) {
       return xScale(d[1]);
	})
	.attr("cy", function(d) {
        return yScale(d[2]);
	})
	.attr("r", function(d) {
		return rScale ((d[2]/(2+d[0])/3) );	// div by 0 risk and scale down for demo
		});

new_chart.selectAll("text")
   .data(dataset2)
   .enter()
   .append("text")
   .text(function(d) {
        return d[1] + "," + d[2];
   })
   .attr("x", function(d) {
        return xScale(d[1]);
   })
   .attr("y", function(d) {
        return yScale(d[2]);
   })
   .attr("font-family", "sans-serif")
   .attr("font-size", "11px")
   .attr("fill", "red");
*/
   
   
// json driven
//////////////

// this is the critical problem in putting graph into correct location

// the ".dropsites" is configured as as svg area in index.html, so #graphNN returns an SVG
// but the gridstack is just setting up divs that are supposed to be populated by
// svgs with this code, and #graphNN returns an html/div reference

// need to test the svg append code here in the index.html version

// for index.html use ".dropsites"

var site = "flex";

if ("flex" != site){
    var graphDropArea = ".dropsites";
    var tgt_graph_slot = "";
}else{
    var graphDropArea = "";
    var tgt_graph_slot = "#graph1";
}

var graph_div_selector = graphDropArea +" "+ tgt_graph_slot;

//`console.log(graph_div_selector);

//var chart_div = d3.select(graph_div_selector);
var chart_div = d3.select(tgt_graph_slot);
//console.log("checking chart_div selector");
//console.log(chart_div);
//var tmp_chart_svg = d3.select(graph_div_selector).append("svg");
var tmp_chart_svg = d3.select("#graph0").append("svg")
                        .append("circle")
                        .attr("cx",30)
                        .attr("cy",30)
                        .attr("r",8)
                        .style("fill","red");
//$("#graph2").append("<p>Jquery select and append</p>");
d3.select("#graph2").append("svg")
                        .append("circle")
                        .attr("cx",30)
                        .attr("cy",30)
                        .attr("r",3)
                        .style("fill","gray");
//console.log("checking chart_svg contents");
//console.log(tmp_chart_svg);


add_data_to_xy_chart("http","localhost:8000","xy", graph_div_selector, data_id);

// will need have chart properties object or similar
function add_data_to_xy_chart(protocol, api_base_url, api_name, tgt_chart_div_selector, sample_id_iterable){
    // this would work as class with defaults
    
    // for now, just do circle plot with series split by size
    var cnt = 2;
    var json_chart = add_new_chart_svg_to_div(tgt_chart_div_selector, chart_w, chart_h);

    
    for (n_id in sample_id_iterable){
            
        // tag each series with sample name or id    
        var selector = "#"+ sample_id_iterable[n_id];
            
        var api_url= build_api_url("http","localhost:8000","xy", sample_id_iterable[n_id]);
            
        plot_series (json_chart, selector, api_url, cnt);
                    
        cnt = cnt + 5;
    }
}

   
function add_new_chart_svg_to_div(div_selector, cht_w, cht_h){
     
    /// this isn't doing what is says - fortuitously there was an svg of same name
    // in index.html which is why that worked
    console.log("Trying to add svg to div_selector", div_selector);
    //var chart_svg = d3.select(div_selector).selectAll("svg")
    //            .enter()
    var chart_svg = d3.select(div_selector)
                .append("svg")
                // ideally want "g" as well
                .attr("width",cht_w)
                .attr("height",cht_h)
				.attr("class", "chartArea")
                //debug marker
                .append("circle").attr("cx",10).attr("cy",10).attr("r",5).style("fill","yellow");
    // debug tracer
    chart_svg.attr("style","outline: 3px dashed blue;");
    console.log(chart_svg);
            
    return chart_svg;
    
    
    /*
    var chartGrid = d3.select(".dropsites").selectAll("svg")
//var chartGrid = chart_area.select("chart_area")
    .data(d3.range(num_charts).map(function() { return {x: chart_w / 2, y: chart_h / 2}; }))
    .enter()
	.append("svg")
	.attr("id",function(d,i){return "svg_graph"+i++;})
	//.append("g")		// we need to group for transform operations, but forces into single columns
    .attr("width", chart_w)
    .attr("height", chart_h)
	.attr("style", "border: 1px rgb(0,100,100);");

    */
}

/*
function add_svg_to_graph_div(graph_div_selector){
    var graph_svg = d3.select(graph_div_selector).selectAll("svg")
        .data(d3.range(num_charts).map(function() { return {x: chart_w / 2, y: chart_h / 2}; }))
        .enter()
        .append("svg")
        // don't do this in function - use the graph area selector with subselection by class
        //.attr("id",function(d,i){return "graph"+i++;})
        //.append("g")		// we need to group for transform operations, but forces into single columns
        .attr("width", chart_w)
        .attr("height", chart_h)
        .attr("style", "border: 1px rgb(0,100,100);");   // this should come from class
    return graph_svg;
}
*/
   
function plot_series(chart_svg_handle, selector_id, api_url, rad) {
// at the moment this naively reads json as x, y duples
// need to add more intelligent parsing
// and also need series property object (color, marker etc)
  
    console.log("in plot series");
    console.log(chart_svg_handle);
  
 	d3.json(api_url, function(error, json){	// this is async, so need to freeze data_id in call **
			if (error) throw error;

			chart_svg_handle.selectAll(selector_id)
				.data(json)
				.enter()
				.append("circle")
				.attr("cx", function(d) {
				   return xScale(d[0]);
				})
				.attr("cy", function(d) {
					return yScale(d[1]);
				})
				.attr("id", selector_id)
				.attr("r", rad);  
	}); 
}	
      
      
// data collection functions   
   
console.log("Checking Vigr");
console.log(Vigr.heartbeat());
   
// example "main"-ish block   
   
// name collision possible here
// with multiple data sources

/*
var xy_values = new Object();	// investigate closures to avoid global
								// will need have an isReady added for async usage

async_store_json_from_api("http","localhost:8000","xy","sample1",xy_values);
async_store_json_from_api("http","localhost:8000","xy","sample2",xy_values);

console.log(xy_values);
setTimeout(function(){console.log(xy_values);}, 500 );
*/
/// set up data access functions   
////////////////////////////////

function build_api_url(prtcl, api_base_url, srvc, data_id){
	var api_url = prtcl+"://"+api_base_url+"/"+srvc+"/"+data_id;
	return (api_url);
}
	
// need to think about how to manage visualisation here
// doing the data storage approach will mandate prefetch
// rather than render during json fetch	
	
function async_store_json_from_api(prtcl, api_base_url, srvc, data_id, store_obj){
	// this is an asynchronous load from url (essentially prefetch)
    // should really integrate with some cache functionality
	// check the isReady value on store_obj before using
	var api_url = build_api_url(prtcl, api_base_url, srvc, data_id);
	
	d3.json(api_url, function(error, json){
		if (error) throw error;
		// should, and where, a check on overwrite be enforced?
		store_obj[data_id] = json;		// this only works on internals
										// because of js approach of pass-by-value
    }); 
}


// display element functionality
////////////////////////////////


function add_drop_tgt_marker_to_svg_handle(svg_handle){
    svg_handle.append("circle")
        .attr("class", "dropMarker")
        // modify to suit
        .attr("r", "5px")
        .attr("cx", function(d) { return d.x + icon_size[0]/2;})
        .attr("cy", function(d) { return d.y + icon_size[1]/2;});
}

function remove_drop_tgt_marker_from_selector(div_selector){    // depends on default dropMarker class
    d3.select(div_selector).select(".dropMarker").remove();
}

function remove_drop_tgt_marker_from_svg_handle(svg_handle){    // depends on default dropMarker class
    svg_handle.select(".dropMarker").remove();
}


// of course, json is runnable, so could dynamically load modules from api server

function create_tooltip(selector, data_obj, intial_state){
    var tooltipDiv = d3.select(selector).append("div")   
        .attr("class", "tooltip")               
        .style("opacity", 0.50); // show this for debugging
    return tooltipDiv;
}



