//
// Vigr simple xy graph model
//
//  (c) Charles Bird, 2016
//

// TODO - still working on this
// should probably get the enclosing elements (fullscreen, delete etc) wrapped into 
// some type of constructor

// tight augmentation - load after Vigr_core

var Vigr = (function (Vigr) {
    
    this.plotXYseries = function (graphObjHandle, dataObjHandle, formatObjHandle) {
    };
    
    // will need to pass a data object, rather than calling api within function
    //
    // also need to think about to pass the underlying svg - is just the handle best
    // or is there meta data to add?
    
    var _plot_series = function (){
        
    };
    
    
function plot_series(chart_svg_handle, selector_id, api_url, rad) {
// at the moment this naively reads json as x, y duples
// need to add more intelligent parsing
// and also need series property object (color, marker etc)
  
    console.log("in plot series");
    console.log(chart_svg_handle);
  
 	d3.json(api_url, function(error, json){	// this is async, so need to freeze data_id in call **
			if (error) throw error;

			chart_svg_handle.selectAll(selector_id)
				.data(json)
				.enter()
				.append("circle")
				.attr("cx", function(d) {
				   return xScale(d[0]);
				})
				.attr("cy", function(d) {
					return yScale(d[1]);
				})
				.attr("id", selector_id)
				.attr("r", rad);  
	}); 
}	
    // export public methods
    return this;

      
})(Vigr || {console.log("Warning - unable load Vigr base module");});
// callout additional dependencies here TODO
// })(Vigr || {console.log("Warning - unable load Vigr base module");});

