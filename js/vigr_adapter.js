//
//  Vigr adapters
//  these provide mapping between the data representation using in the visualisation
//  engine and (relatively) arbitrary formats from file or API
//  
//  the initial implementation supports predefined settings
//  but dynamic & user-defined options can be supported`
//
//  (c) Charles Bird, 2016
//


// there are of course a range of possible adaptions
// we'll start at the easy end and leave hooks for user defined options
// (adapters are json, so could put functions, but serialisation needs
//  care)
//
// i)   label transforms
// ii)  arithmetical (eg scaling, normalisation, re-origin)
// iii) low level analytics (eg. mean, sd)

// tight augmentation - load after Vigr_core

// need to be careful using this wrt capability detection
// may need to provide downgrade functionality

var _native_adapter = {x:"x",y:"y",z:"z",t:"t"};


var Vigr = (function (Vigr) {
    
    this.openData = function( fileObj ){
        
        var reader = new FileReader();
         
        // could use an onload closure to undertake the transformation
        // if the adapter object is supplied
        // but would need robust error handling
        
        // Closure to capture the file information.
        reader.onload = (function(theFile) {
                
                return function(e) {
                    
                    console.log(e.target.result);   // this will need putting into object
                                                    // also escaping
                }
              // Render thumbnail.
              //var span = document.createElement('span');
              //span.innerHTML = ['<img class="thumb" src="', e.target.result,
              //                  '" title="', escape(theFile.name), '"/>'].join('');
              //document.getElementById('list').insertBefore(span, null);
            })(fileObj);    
            
        reader.readAsText(fileObj);  
    };
    
    
    // this function has a look at the (start if big) data
    // in order to make some best guess on contents to drive
    // adapter selection
    
    // we should actually do the "in-data" bit on blobs then can generalise for 
    // http fetches etc
    this.investigateData = function( fileObj ){
        
        // look at application/type is present
        // check extension
        
        // these will to search against the adapter objects
        
        // would access to path information be useful??
        
        // check size, if small ingest and attempt to parse
        // if large, read as blob and slice 
                
        var reader = new FileReader();
         
        reader.onload = (function(theFile) {
                return function(e) {
                    console.log(e.target.result);   // this will need putting into object
                                                    // also escaping
                }
            })(fileObj);    
        
        reader.readAsText(fileObj);  
    };
    
    
    // this is really a dataObj method, but put here in js
    // alternative model would be to do this in API (since 
    // presumably only calling on analytics results) where
    // probably easier to do some more intelligent data-driving
    //
    this.transformDataLabels = function(adapter_name, data_src){
        var tmp = null;
               
        return (tmp);
    }
    
    // capability determination
    ///////////////////////////
    //
    // can a particular data set be used/displayed after transformation
    // - essentially derive from reverse lookup into adapter defined for data
    // e.g. if an adapter can map something onto an {x,y} duple, any xy-type graph
    // can be displayed
    
    this.canDisplay = function ( data_adapterObj, graphReqt){
        
        // stub - otherwise nothing will display
        return(true);
    }
       
    // adapter admin functions
    //////////////////////////
    
    // will need to hook into read/write functions from API
    // and access to protected variables
    
    this.loadAdapter = function(adapter_name, api_service){
        return (null);
    }
    
    // ?? track adapter origin - otherwise these two fucntions
    // would provide a copy service, if that's a problem
    this.saveAdapter = function(adapter_name, api_service){
        
    }
    
    // this will have to be integrated with gui display
    this.newAdaper = function(adapter_name, adapter_map){
        
    }
    
// lifted display functions from core for modification   
    
/*    
this.populateSelectPanel = function ( tgt_panel_class ){
    // generate a scrollable list with a radio button, name, and (abbreviated) mapping 
      
    var slctr = tgt_panel_class + "> .tool_panel_content";
        
    _buildAdapterSelectList( slctr);
    
    // bit of work to do on alignment
      
    // have to faff around getting quotes escaped properly here, 
	// doesn't work properly with string in prepend
    
    // could replace function names with call to handler here
    var func_str = "<button class='panel_btn' onclick='Vigr.confirmAdapterSelection(";                        
    func_str += "\""+tgt_panel_class+"\"";
    func_str += ")'> Confirm Selection</button>";
        
    $( tgt_panel_class+"> .tool_buttons").prepend(func_str);
    
    var func_str = "<button class='panel_btn' onclick='Vigr.createAdapter(";                        
    func_str += "\""+tgt_panel_class+"\"";
    func_str += ")'> Create new adapter</button>";
    
    $( tgt_panel_class+"> .tool_buttons").prepend(func_str);
*/  
    /*
    // make these choices on per adapter basis
    var func_str = "<button class='panel_btn' onclick='Vigr.cloneAdapter(";                        
    func_str += "\""+tgt_panel_class+"\"";
    func_str += ")'> Create new adapter</button>";
    
    var func_str = "<button class='panel_btn' onclick='Vigr.cloneAdapter(";                        
    func_str += "\""+tgt_panel_class+"\"";
    func_str += ")'> Create new adapter</button>";
    */
         
  //  $( tgt_panel_class+"> .tool_buttons").prepend(func_str);
    
    // TODO need to override cancel function to reset the checkboxes to correct values
    //******  
  //}

  /*
    var _buildAdapterSelectList = function ( slctr){
          // this section is the bit that needs to be split out to allow refresh
        
        // could try to be cleverer here, but called so infrequently
        // that slow but working will do
        // delete any prior contents
        $(slctr).empty();
        $(slctr).append("<table class='adapter_list'></table");
        
        // add style to th for width <td style="width:130px">    TODO
        // there's some faff here on picking up width in css
        // also possible move to pure css table
        
        $(slctr).append("<thead style='width:100%'><tr>"+
                        "<th class='ad_use' style='width:15%'>Use?</th>"+
                        "<th class='ad_use' style='width:10%'></th>"+       // edit placeholder
                        "<th class='ad_use' style='width:10%'></th>"+       // clone
                        "<th class='ad_name' style='width:20%'>Name</th>"+
                        "<th class='ad_name' style='width:45%'>Mapping</th>"+
                        "</tr></thead>");
                                                    
        // style the table body here for the moment 
        $(slctr).append("<tbody style='overflow-y: scroll; overflow-x: hidden;'></tbody");
        
        // build the list dynamically
        // ?? sorting functionality
        for ( adptr_id in _all_libraries){    // pulls all the keys = lib names
            $(slctr+" > tbody").append( _build_select_adapter_table_row( adptr_id ) );
        }
        
  };
  
   var _build_select_adapter_table_row = function (adptr_id){
        
        // this will interact with the library/dataset representation
        
        var cutoff  = 40;   // this will be dynamic
        var tr_ele = "<tr>";
       
        // ensure that the id is unique on page hence need distinct prefix
        // and protect against numeric lib name (id must start with alpha)
        var chk_id = _use_adptr_prefix + adptr_id;
        
        // check if adptr_id is used with active dataset to check correct radio button
        
        // TODO *****
        if ( _current_libs.indexOf( lib_name ) > -1 ){
            tr_ele += "<td class='ds_use'><input type='radio' id='"+chk_id+"' checked></td>";
        }else{
            tr_ele += "<td class='ds_use'><input type='radio' id='"+chk_id+"'></td>";
        }
        
        // add edit and clone buttons
        // need onclick call
        tr_ele += "<td class='ds_use'><button id='"+chk_id+"' checked>Edit</td>";
        tr_ele += "<td class='ds_use'><button id='"+chk_id+"' checked>Clone</td>";
                
        // name
        tr_ele += "<td>"+adptr_id_name+"</td>";
        
        // description, with length sensitive popup - interaction with html elements/styling
        // how much detail should we get into dataset popups??
        // 
        // balance between seperate description and descriptive name
        var adptr_desc = _all_adapters[adptr_id].desc;
        
        // do something on length here idc TODO
        if ( lib_desc.length > cutoff){
            tr_ele += "<td>" + lib_desc + "</td>";    // TODO
        }else{
            tr_ele += "<td>" + lib_desc + "</td>";
        }
        
        
        // TODO FROM HERE *****
// is string or array better here
// suspect array, but then need supporting function to generate string of IDs for display
// need to error check - probably easiest in JSON parse sanitise function in load function
        var lib_dsets = _all_libraries[lib_name].dsets;
        // do something on length here
        if ( lib_dsets.length > cutoff){
            tr_ele += "<td>" + lib_dsets + "</td>";    // TODO
        }else{
            tr_ele += "<td>" + lib_dsets + "</td>";
        }
        
        // close out line
        tr_ele += "</tr>";
            
        return(tr_ele);
   }

   // can we get slctr value from somewhere?
   // - should actually define that when attaching to event queue
    this.updateAdapterListHandler = function (e){
        console.log("updateAdapterListHandler called");
        console.log("called with function class: "+e.data.func_class);
        _buildLibSelectList(e.data.func_class);
    }
    
    this.editAdapter = function (adptr_id){
        var tmp_adapter = _all_adapters[adptr_id];
        // push this into  separate function display/edit display
        // which is shared with clone
        
        // check return value from panel
     // if confirm, _all_adapters[adptr_id] = tmp_adapter 
    };
    
    this.cloneAdapter = function (adapter){
        var tmp_adapter = _all_adapters[adptr_id];
        // change the name of temp adapter here
        
        // push into display/edit panel
        
        // check return value from panel
     // if confirm, _all_adapters[new_id] = tmp_adapter 
               
    }
  */
    
    // export public methods
    return (this);
      
})();

//////})(Vigr || {console.log("Warning - unable load Vigr base module");});
// callout additional dependencies here TODO
// })(Vigr || {console.log("Warning - unable load Vigr base module");});

