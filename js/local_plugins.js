// these are the panel management functions

// we should really do this as some form of closure, so that we can associate a copy 
// of the function with state to each panel

// need to pick the registry up as global in this model??
// probably better to wrap into Vigr (code reuse and access)


// can avoid all the faff with passing func name by generating ad-class on the
// fly for each tool/graph instance

// need to work out what is passed in onclick action - does context pass
// or do we need to extract and pass classname as call??

//***************************************************************
// user data escape

// TODO need to consider global approach to escaping user data
// initial preference would be to sanitise on entry
// with double check on returned values from user accessible stores
// such as local storage and files
// use a wrapper to make addition of html/js/sql checks easier

function _escapeString(str) {   

    var ret_str = _escapeHtml(str);
    
    return (ret_str);
}


// Use the browser's built-in functionality to quickly and safely html escape
// the string  --- ??? IS THIS STILL VULNERABLE TO CODE?? TODO
function _escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
}

// UNSAFE with unsafe strings; only use on previously-escaped ones!
function _unescapeHtml(escapedStr) {
    var div = document.createElement('div');
    div.innerHTML = escapedStr;
    var child = div.childNodes[0];
    return child ? child.nodeValue : '';
}

// end user data escape
// ******************************************

function showLocalStorage(){
    console.log("Showing all localStorage values");
    for ( var i = 0, len = localStorage.length; i < len; ++i ) {
            console.log(localStorage.key(i)+":"+localStorage.getItem( localStorage.key(i) ));
    }
}
	
/*
function confirm_selection(func_name){
    Vigr.confirmSelection(func_name);
}
*/

/*  plugin */
/*
(function($){
	$.fn.show_panel = function(panel_id){
        console.log("show_panel plugin");
        $( panel_id ).show( "slide");
    };
})(jQuery);

(function($){
	$.fn.hide_panel = function(panel_id){
        console.log("hide_panel plugin");
        $( panel_id ).hide( "slide");
    };
})(jQuery);
*/
 
/*
    // callback function to bring a hidden box back
    function callback() {
      setTimeout(function() {
        $( "#effect" ).removeAttr( "style" ).hide().fadeIn();
      }, 1000 );
    };
*/
  
  
  /*
   * hoverIntent | Copyright 2011 Brian Cherne
   * http://cherne.net/brian/resources/jquery.hoverIntent.html
   * modified by the jQuery UI team
   */
   
  $.event.special.hoverintent = {
    setup: function() {
      $( this ).bind( "mouseover", jQuery.event.special.hoverintent.handler );
    },
    teardown: function() {
      $( this ).unbind( "mouseover", jQuery.event.special.hoverintent.handler );
    },
    handler: function( event ) {
      var currentX, currentY, timeout,
        args = arguments,
        target = $( event.target ),
        previousX = event.pageX,
        previousY = event.pageY;
 
      function track( event ) {
        currentX = event.pageX;
        currentY = event.pageY;
      };
 
      function clear() {
        target
          .unbind( "mousemove", track )
          .unbind( "mouseout", clear );
        clearTimeout( timeout );
      }
 
      function handler() {
        var prop,
          orig = event;
 
        if ( ( Math.abs( previousX - currentX ) +
            Math.abs( previousY - currentY ) ) < 7 ) {
          clear();
 
          event = $.Event( "hoverintent" );
          for ( prop in orig ) {
            if ( !( prop in event ) ) {
              event[ prop ] = orig[ prop ];
            }
          }
          // Prevent accessing the original event since the new event
          // is fired asynchronously and the old event is no longer
          // usable (#6028)
          delete event.originalEvent;
 
          target.trigger( event );
        } else {
          previousX = currentX;
          previousY = currentY;
          timeout = setTimeout( handler, 100 );
        }
      }
 
      timeout = setTimeout( handler, 100 );
      target.bind({
        mousemove: track,
        mouseout: clear
      });
    }
  };


// this plugin styles a css popup menu for a text entry field
/*
(function($){
	$.fn.style_select_dropdown = function(){
        // config options
        var fade_time = 100;
	
        return this.each(function(){
			var obj = $(this)
			obj.find('.selectfield').click(function() { //onclick event, 'list' fadein
                obj.find('.selectlist').fadeIn(fade_time);
			
                $(document).keyup(function(event) { //keypress event, fadeout on 'escape'
                    if(event.keyCode == 27) {
                    obj.find('.selectlist').fadeOut(fade_time);
				}
			});
			
			obj.find('.selectlist').hover(function(){ },
				function(){
					$(this).fadeOut(fade_time);
				});
			});
			
			obj.find('.selectlist li').click(function() { // onclick event, change field value with selected 'list' 
                                                    // item and fadeout 'list'
			obj.find('.selectfield')
				.val($(this).html())
                // these need to match overall styling - should pick from css for class  ****
				.css({
					'background':'yellow',
					'color':'#333'
				});
			obj.find('.selectlist').fadeOut(fade_time);
			});
		});
	};
})(jQuery);


// this styles a popup with click opening new window
// 

(function($){
	$.fn.style_open_chooser_menu = function(){
        // config options
        var fade_time = 100;
		
        return this.each(function(){
			var obj = $(this)
			obj.find('.field').click(function() { //onclick event, 'list' fadein
			obj.find('.list').fadeIn(fade_time);
			
			$(document).keyup(function(event) { //keypress event, fadeout on 'escape'
				if(event.keyCode == 27) {
				obj.find('.list').fadeOut(fade_time);
				}
			});
			
			obj.find('.list').hover(function(){ },
				function(){
					$(this).fadeOut(fade_time);
				});
			});
	
// need to change this functionality to call the required function    
			obj.find('.list li').click(function() { // onclick event, change field value with selected 'list' 
                                                    // item and fadeout 'list'
                obj.find('.field')
                    .val($(this).html())
                    // these need to match overall styling - should pick from css for class  ****
                    .css({
                        'background':'#fff',
                        'color':'#333'
                    });
                obj.find('.list').fadeOut(fade_time);
                });
		});
	};
})(jQuery);

*/
