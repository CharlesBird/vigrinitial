import os
import re
#import split_reads2
import sys
import glob
#import pipeline_functions2
import subprocess


# don't need to set pRESTO path on this system
# as added to path during installation

class study:

    _srr_suffix = '_1.fastq'        # quick hack to find one strand of SRR read

    def __init__( self, study_name, study_directory, force ):
        if ( study_name and study_directory):
            self.force = force
            self.runs = list()
            self.tempdirs = dict()
            self.name = study_name
            self.base_dir = study_directory
            self.primer_dir = self.base_dir
            self.build_directories()
            self.build_filenames()
        else:
            print ('Usage: foo.py study_name directory')
            raise ValueError("No study name &/or directory given")
    
    def set_q_threshold(self, qscore):
        self.qscore = qscore
        
    def set_min_assembly_length( self, min_len):
        self.min_len = min_len
    
    def set_primer_base_dir (dir):
        if not os.path.exists(dir):
            raise ValueError("Primer directory ",dir," not found")
        else:
            self.primer_dir = dir
    
    def set_strand_1_primer( self, primer_fname):

        print (self.primer_dir)
        print (primer_fname)
 
        self.primer_strand1 = os.path.join( self.primer_dir, primer_fname)
        
        if not os.path.isfile( self.primer_strand1):
            raise ValueError("set_strand_1_primer: Can't access strand 1 primer: ",self.primer_strand1 )
    
    def set_strand_2_primer( self, primer_fname ):
    
        self.primer_strand2 = os.path.join( self.primer_dir, primer_fname)
        
        if not os.path.isfile( self.primer_strand2 ):
            raise ValueError("set_strand_2_primer: Can't access strand 2 primer: ",self.primer_strand2 )
        
            
    def build_directories( self ):
        print ("finding studies, based on files ending in _1.fastq")
        # need to use OS path
        for file_name in glob.glob( os.path.join(self.base_dir,"*") ):
            pat = re.compile('(^.*)\_1.fastq$')     ## make sure we skip compressed or foo.fastq.orig with $
            seq_collection = pat.search( file_name )
            if seq_collection:
                (pth, nm) = os.path.split( seq_collection.group(0) )
                (srr, dummy) = nm.split('_')    # crude but works for now
                self.runs.append(srr)        # add the srrXXX to a list
        print ("Building temporary directories for following studies")        
        
        #print (self.studies)
        ## add check for _2 idc

        for run in self.runs:
            strand2 = os.path.join(self.base_dir,run+"_2.fastq")
            if not os.path.isfile(strand2):
                raise ValueError("Can't find matching _2.fastq for ", run)
            
            print (run)
            srr_temp_dir = os.path.join( self.base_dir, run+"_tmp" ) ####
            
            print (srr_temp_dir)
            if not os.path.exists( srr_temp_dir):
                os.makedirs( srr_temp_dir )
            self.tempdirs[ run ] = srr_temp_dir
        
        print ()
        self.clean_dir = ( os.path.join( self.base_dir, "clean_seq" ))
        print (self.clean_dir,"will hold all cleaned sequences" )    
        if not os.path.exists( self.clean_dir ):
                os.makedirs( self.clean_dir )
     
    def process_study (self):
        for r in self.runs:
            print ("Processing run ", r)
            if os.path.isfile(os.path.join( self.base_dir, "stop.pipe")):
                print ("Stopping pipeline early because found 'stop.pipe' file")
                return
            self.pRESTO_drop_low_quality_both_strands (r)
            if os.path.isfile(os.path.join( self.base_dir, "stop.pipe")):
                print ("Stopping pipeline early because found 'stop.pipe' file")
                return
            self.pRESTO_mask_primers (r)
            if os.path.isfile(os.path.join( self.base_dir, "stop.pipe")):
                print ("Stopping pipeline early because found 'stop.pipe' file")
                return
            self.pRESTO_assemble_pair (r)      # minlen here refers to minimum overlap
                                               # not min length of sequence
        #self.pRESTO_drop_short_assemblies ( run, 200 ) # see above
    
        
    def build_filenames( self ):
        pass


    def pRESTO_drop_low_quality_both_strands ( self, run  ):   ## tidy idc
        strand1 = os.path.join(self.base_dir, run+"_1.fastq")
        strand2 = os.path.join(self.base_dir, run+"_2.fastq")
        
        print (run,": removing low quality (Q<",self.qscore,") reads from both strands")

        # strand 1
         
        if (not self.force) and os.path.isfile(os.path.join( self.tempdirs[run], 'R1_trimqual-pass.fastq')):  # skip
            print ("Skipping strand 1 as output file exists - use force or remove to overwrite")
        else:
            cmd = 'FilterSeq.py trimqual -s '
            cmd += strand1 + ' -q ' + str(self.qscore) +' --nproc 4 --outname R1 --outdir ' 
            cmd += self.tempdirs[run]
            os.system(cmd)
            
        if os.path.isfile(os.path.join(self.base_dir, "stop.pipe")):
                print ("Stopping pipeline early because found 'stop.pipe' file")
                return
            
        # strand 2
        if (not self.force) and os.path.isfile(os.path.join( self.tempdirs[run], 'R2_trimqual-pass.fastq')):  # skip
            print ("Skipping strand 2 as output file exists - use force or remove to overwrite")
        else:
            cmd = 'FilterSeq.py trimqual -s '
            cmd += strand2 + ' -q ' + str(self.qscore) +' --nproc 4 --outname R2 --outdir ' 
            cmd += self.tempdirs[run]
            os.system(cmd)   ## cmd replacement
    
    def pRESTO_mask_primers ( self, run  ):   ## tidy idc
        
        print (run,": Masking primers")
                
        # strand 1
        
        if (not self.force) and os.path.isfile(os.path.join( self.tempdirs[run], 'R1_primers-pass.fastq')):  # skip
            print ("Skipping strand1 as output file exists - use force or remove to overwrite")
        else:
            cmd = 'MaskPrimers.py align -s '
            cmd += os.path.join(self.tempdirs[run], 'R1_trimqual-pass.fastq')
    #        cmd += ' -p '+self.primer_strand1
            cmd += ' -p '+self.primer_strand2   ## might have strands/primers reversed
            cmd += ' --outdir ' + self.tempdirs[run]  # new line
            cmd += ' --outname R1'
            cmd += ' --mode cut --maxerror 0.2 --nproc 4 --log PrimerLogR1.log'
            os.system(cmd)   ## cmd replacement
        
        if os.path.isfile(os.path.join( self.base_dir, "stop.pipe")):
                print ("Stopping pipeline early because found 'stop.pipe' file")
                return
            
        # strand 2
        if (not self.force) and os.path.isfile(os.path.join( self.tempdirs[run], 'R2_primers-pass.fastq')):  # skip
            print ("Skipping strand2 as output file exists - use force or remove to overwrite")
        else:
            cmd = 'MaskPrimers.py align -s ' + os.path.join(self.tempdirs[run], 'R2_trimqual-pass.fastq')
    #        cmd += ' -p '+self.primer_strand2
            cmd += ' -p '+self.primer_strand1   ## ??? reversed strands
            cmd += ' --outdir ' + self.tempdirs[run]  # new line
            cmd += ' --outname R2'
            cmd += ' --mode cut --maxerror 0.2 --nproc 4 --log PrimerLogR2.log'
            os.system(cmd)   ## cmd replacement
        
        
    def pRESTO_assemble_pair (self, run ):
    
        clean_fastq = run+".fastq"
        
        print ("Assembling pairs")
        if (not self.force) and os.path.isfile(os.path.join( self.clean_dir, clean_fastq )):  # skip
            print ("Skipping assembly as output file exists - use force or remove to overwrite")
        else:
            cmd = 'AssemblePairs.py align'
            cmd += ' -1 '+ os.path.join( self.tempdirs[run], 'R1_primers-pass.fastq')
            cmd += ' -2 '+ os.path.join( self.tempdirs[run], 'R2_primers-pass.fastq')
            cmd += ' --coord sra --rc tail --maxerror 0.05 --minlen 10 --nproc 4'
            cmd += ' --log AssembleLog.log'
            cmd += ' --outdir '+self.clean_dir
            cmd += ' --outname '+clean_fastq
            os.system(cmd)   ## cmd replacement
                
    def pRESTO_drop_short_assemblies ( self, run ):
        pass

        
        
        
def main(argv):     ## can tweak
    if ( 3 > len(argv) ):
        print ('usage: foo.py study_name directory [force]')
        print ('requires pRESTO > 0.5.2')
        print ('touch "stop.pipe" in directory with raw files to break on next complete step')
        print ("received ", len(argv)," command line options")
        exit
    else:
        force = False
        if 4 == len(argv):
            force = True
            print ("Enabling overwrite of existing intermediate/output files")
        stdy = study (argv[1], argv[2], force)
        stdy.set_q_threshold(20)
        stdy.set_min_assembly_length(200)
        ## stdy.set_primer_base_dir (dir)       ## optional, otherwise looks in directory with raw fastq
        stdy.set_strand_1_primer( "tipton_FwdPrimers5-3.fasta" )
        stdy.set_strand_2_primer( "tipton_RevPrimers5-3.fasta" )
        stdy.process_study();


def run_illumina_pipeline(folder, species, overall_quality_threshold):
    for file_name in glob.glob(folder + '*_1.fastq'):
        illumina_pipeline(file_name, species, overall_quality_threshold)
    

def illumina_pipeline(infile, species, overall_quality_threshold):
    pat = re.compile('(^.+)/.*?/(.*?)\.fastq')
    info = pat.search(infile)
    folder = info.group(1)
    name = info.group(2)
    
    results_dir = folder + '/' + name
    cleaned_dir = results_dir + '/cleaned'
    vidjil_dir = results_dir + '/vidjil'
    imgt_dir = results_dir + '/imgt'
    cleaned = cleaned_dir + '/' + name + '_cleaned.fasta'
    imgt_input = imgt_dir + '/' + name
    split_dir = cleaned_dir + '/split'

    #make output directory
    os.system('mkdir ' + results_dir)
    os.system('mkdir ' + cleaned_dir)
    os.system('mkdir ' + vidjil_dir)
    os.system('mkdir ' + imgt_dir)
    if not glob.glob(cleaned):
        #trim reads by quality
        os.system('python2.7 /d/as5/u/la001/software/pRESTO_v0.4/FilterSeq.py trimqual -s ' + infile + ' -q 20 --nproc 4 --outname R1 --clean --outdir ' + cleaned_dir + ' &> ' + cleaned_dir + '/quality_R1.txt') 
        os.system('python2.7 /d/as5/u/la001/software/pRESTO_v0.4/FilterSeq.py trimqual -s ' + infile[:-7] + '2.fastq -q 20 --nproc 4 --outname R2 --outdir ' + cleaned_dir + ' &> ' + cleaned_dir + '/quality_R2.txt') 
        print ('reads trimmed')
        
        #mask primers
        os.system('python2.7 /d/as5/u/la001/software/pRESTO_v0.4/MaskPrimers.py align -s ' + cleaned_dir + '/R1_trimqual-pass.fastq -p /d/as7/scratch/u/la001/SRP057017/artifacts/forward.fasta --mode cut --maxerror 0.2 --nproc 4 --log PrimerLogR1.log --clean &> ' + cleaned_dir + '/R1_mask_primers.txt')
        os.system('python2.7 /d/as5/u/la001/software/pRESTO_v0.4/MaskPrimers.py align -s ' + cleaned_dir + '/R2_trimqual-pass.fastq -p /d/as7/scratch/u/la001/SRP057017/artifacts/reverse.fasta --mode cut --maxerror 0.2 --nproc 4 --log PrimerLogR2.log --clean &> ' + cleaned_dir + '/R2_mask_primers.txt') 
        print ('primers removed')    

        #join reads
        os.system('python2.7 /d/as5/u/la001/software/pRESTO_v0.4/AssemblePairs.py align -1 ' + cleaned_dir + '/R1*primers-pass.fastq -2 ' + cleaned_dir + '/R2*primers-pass.fastq --coord sra --rc tail --maxerror 0.05 --minlen 10 --nproc 4 --log AssembleLog.log --outname Assembled --clean &> ' + cleaned_dir + '/assembled.txt')
        print ('reads joined')

        if glob.glob(cleaned_dir + '/Assembled_assemble-pass.fastq') and not glob.glob(cleaned_dir + '/QC.unpaired.trimmed.fastq'):
                os.system('perl /d/as5/u/la001/software/FaQCs.pl -u ' + cleaned_dir + '/Assembled_assemble-pass.fastq' + ' -d ' + cleaned_dir + ' -qc_only') 
        print ('quality control graphs made')      
   
        if glob.glob(cleaned_dir + '/Assembled_assemble-pass.fastq') and not glob.glob(cleaned):
            subprocess.call('cat ' + cleaned_dir + '/Assembled_assemble-pass.fastq | split -l 40000000 -a 2 - ' + split_dir, shell = True)
            for f in glob.glob(split_dir + 'a*'):
               subprocess.call('/d/user5/wlees01/bin/usearch -threads 15 -fastq_filter ' + f + ' -fastq_maxee ' + overall_quality_threshold + ' -fastaout ' + f + '.fasta &> ' + cleaned_dir + '/usearch.txt' , shell = True)
            subprocess.call('cat ' + split_dir + 'a*.fasta > ' + cleaned, shell = True)
            print ('Reads filtered by quality')

    #gather into clones
    if not glob.glob(vidjil_dir + '/output.log'):
        os.system('/d/as2/u/wlees01/vidjil/vidjil -c clones -o ' + vidjil_dir + \
                  ' -G /d/as2/u/wlees01/vidjil/germline/' + species + '/IGH -y all -z 0 -r 2 -d -w 60 ' + \
                  cleaned + ' > ' + vidjil_dir + '/output.log')
        print ('Reads gathered into clones')

    #split fasta output from vidjil into files processable by IGMT, determine number of files.
    if not glob.glob(imgt_input+'*'):
        num_files = pipeline_functions2.split_vidjil_output(vidjil_dir + '/' + name + '_cleaned.vdj.fa', imgt_input)
        print ('IMGT input files prepared')

    #extract CDR3s
    if glob.glob(imgt_input + '*') and not glob.glob(imgt_input + '1.fasta.cdr'):
        for seq_file in glob.glob(imgt_input + '*'):
            os.system('python /d/as5/u/la001/software/AbMiningToolbox/cdr3_search.py ' + seq_file)

    #remove fastq files
    if glob.glob(imgt_input + '*'):
        os.system('rm -r ' + cleaned_dir + '/*.fastq')
      
   
if __name__=="__main__":
    main(sys.argv)  
