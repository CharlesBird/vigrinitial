#!/usr/bin/python3
# 5 Apr 2016, Charles Bird
''' security functions '''
''' this implementation starts from a baseline of all passwords hashed against '''
''' bcrypt 2b, so migration is relatively easy to another MCF format system '''


import bcrypt   # this is the default for this project, using 2b
                # potential extensions to 
                # scrypt
                # yescrypt
                # argon2
#import pbkdf2  # only if mandated by auditor
import hashlib
import hmac
import re
import sqlite3  # we'll use this for the moment as a lightweight test environment
                # later we can move across to MySQL
                    
import logging  # we'll be using this more generally, but we should have a separate sy stream
import string
import os
import sys

from enum import Enum, unique   # python > 3.4

@unique
class PWstate(Enum):
    FAIL        = 0
    MATCH       = 1
    MIGRATE     = 2
    COMPLEX_OK  = 3
    COMPLEX_FAIL= 4
    TMPLT_OK    = 5
    TMPLT_FAIL  = 6

    
class PWdefaults():
    # ulds are character classes (upper/lower/digit/special)
    # - might amend to for the common specials (., space !) as subtype
    # PERM is number of class permutations required 
    MIN_CHAR    = {'u':1,'l':1,'d':1,'s':1}
    MIN_LEN     = 8
    MAX_LEN     = 1024  # this is to block DDOS
    PERMUTE     = 3
    
class AuthDefaults():
    MAX_ATTEMPTS    = 10
    DEFAULT_LOCKOUT = {0:0,5:10,10:60}  # isn't the default while testing development
    

    
class PWcomplexity:
    ''' standard complexity stuff (length, number required of upper/lower/digit/special) '''
    ''' each class reqt must be <= min_len as must total over all classes '''
    ''' permute sets how many out of char classes need to be met (0-4)'''
    ''' if a char_req is supplied missing a class(es), default is used, and '''
    ''' permute set to number of supplied classes '''
    ''' An isolated missing permute will be set to default'''
    ''' standalone, but recommend using with the pathwell modules for higher security passwords '''
            
    def __init__(self, *,   min_len=PWdefaults.MIN_LEN, 
                            char_req={  'u':PWdefaults.MIN_CHAR['u'],
                                        'l':PWdefaults.MIN_CHAR['l'],
                                        'd':PWdefaults.MIN_CHAR['d'],
                                        's':PWdefaults.MIN_CHAR['s'],
                                        'permute':PWdefaults.PERMUTE}):
    
        # given the importance of this code in setting passwords
        # we'll check rigorously
    
        self._fail_reason = ""
        
        if not isinstance(min_len, type(1)):
            raise TypeError("Attempting to set min password length with wrong type")
            
        if ( min_len < 0):
                raise ValueError("Negative minimum password length: {:d}".format(min_len))
                
        if ( min_len > PWdefaults.MAX_LEN):
            raise ValueError("Required min length ({:d}) exceeds maximum phrase "
                                "length ({:d})".format(min_len, PWdefaults.MAX_LEN))
        else:   
            self._min_len = min_len
                    
        if type(char_req) is not dict:
            raise TypeError("char_req does not contain required dict")
        else:
            for ch in char_req:
                if not isinstance(char_req[ch],type(1)):
                    raise TypeError("Supplied char_req contains non-integer value ",
                            "for character class requirement")
            self._char_req  = char_req.copy()   # must do shallow copy here
            total_class_len = 0
            n_missing       = 0
            n_sup_classes   = 0
                
        # note that we ignore any undefined symbols in the char_req parameter  
        for chr_cls in ('u', 'l', 'd', 's'):
            if chr_cls not in char_req:     # check for missing values
                self._char_req[chr_cls] = PWdefaults.MIN_CHAR[chr_cls]
                n_missing += 1
            else:
            # debugging
                # print ("chr cls: ",chr_cls," -> ", self._char_req[chr_cls])
                if (self._char_req[chr_cls] < 0 or self._char_req[chr_cls] > self._min_len):
                    raise ValueError("Can't set class {:s} to "
                                    "{:d}".format(chr_cls, self._char_req[chr_cls]))
                n_sup_classes += 1
            total_class_len += self._char_req[chr_cls] 
            

        if 'permute' not in char_req:
            if ( 0 == n_missing):
                self._char_req['permute'] = PWdefaults.PERMUTE
## logging
                # print ("XX missing permute, no missing char class => permute = {:d}".format(self._char_req['permute']))
            else:
                if ( 4 == n_missing):   ## essentially default case
                    self._char_req['permute'] = PWdefaults.PERMUTE
                  #  print ("XX missing permute, {:d} char class +ve values supplied "
                  #      "=> permute = {:d}".format(n_sup_pos_classes, self._char_req['permute']))
                else:
                    self._char_req['permute'] = n_sup_classes
                  #  print ("XX missing permute, {:d} char class values supplied "
                  #      "=> permute = {:d}".format(n_sup_pos_classes, self._char_req['permute']))
                  
        ## should run this code anyway as check
        
        if not isinstance(self._char_req['permute'], int):
            raise TypeError("Attempting to set permute with a non-integer")
            
        if ( (self._char_req['permute'] <0) or (self._char_req['permute'] > 4)):
            raise ValueError("Attempting to set permute to illegal "
                                "value ({:d})".format(self._char_req['permute']))
        elif ( total_class_len > self._min_len ):
            raise ValueError("Total characters by class ({:d}) exceeds "
                           "min password length ({:d})".format(total_class_len,self._min_len))

        
    def isComplexityOK(self, clearpw):      ## will need to return tuple of state and reason
    
    # debug  ******
        #print ("\n===")
        #print ("Constructor instance var\n")
        #print ("Min len: ", self._min_len)
        #print ("Class req: ",self._char_req)
        #print ("pw type: ", type(clearpw))
        
        # need to track all reasons for complexity failures to pass back
        ret_state = PWstate.COMPLEX_FAIL    ## is this the best way - all tests below are negatives
        msg_collect = []
        char_count  = {}
        char_msg    = {'l':"lower case", 'u':"upper case", 'd':"number", 's':"special (!?#+% etc)"}
        isOK = {'min_len':False, 'max_len':False, 'u':False, 'l':False, 'd':False, 's':False, 'permute':False}
        
        if not isinstance(clearpw,type("string")):
            if isinstance(clearpw,type(None)):
                clearpw = ""
            else:
                raise TypeError("isComplexityOK called with non-string password: )", type(clearpw))
   
        if ( len(clearpw) < self._min_len ):
            msg_collect.append("".join( ("Pass phrase length (",str( len(clearpw) ),
                                        ") below required minimum of ",str(self._min_len), " characters" )))
        else:
            isOK['min_len'] = True
        
        if ( len(clearpw) > PWdefaults.MAX_LEN ):
            msg_collect.append("".join( ("Pass phrase length (",str( len(clearpw) ),
                                        ") exceeds maximum of ",str(self._min_len) )))
        else:
            isOK['max_len'] = True
            
        for chr_cls in ('u', 'l', 'd', 's'):
            char_count[chr_cls] = 0
            
        if (len(clearpw) > 0 ):
            for c in clearpw:       ## unicode
                if (re.match(r'[A-Z]', c )):
                    char_count['u'] += 1
                elif (re.match(r'[a-z]', c )):
                    char_count['l'] += 1
                elif ( c.isdigit() ):
                    char_count['d'] += 1
                elif (re.match(r'[^0-9a-zA-Z]', c )):
                    char_count['s'] += 1
                else:
                    raise RuntimeError("Reached impossible condition in isComplexityOK (char {:s})".format(c))
        
        perm_count = 0
        for chr_cls in ('u', 'l', 'd', 's'):
            if ( char_count[chr_cls] >0 ):
                perm_count += 1
            if ( char_count[chr_cls] < self._char_req[chr_cls] ):
                msg_collect.append("".join( ("Pass phrase contains only ", str(char_count[chr_cls]),
                                        " ", char_msg[chr_cls], " characters - needs at least ",
                                        str(self._char_req[chr_cls]) )))
            else:
                isOK[chr_cls] = True
        
        if ( perm_count < self._char_req['permute'] ):
            tmp_str = "".join( ("Pass phrase needs to contain at least ", str(self._char_req['permute']),
                                    " from the following character types: ") )
                                    
            for chr_cls in ('u', 'l', 'd', 's'):
                if ( self._char_req[chr_cls] > 0 ):
                    tmp_str = "".join( (tmp_str, str(self._char_req[chr_cls]), ", "))
            tmp_str = "".join( (tmp_str, "it only contains ",str(perm_count)))
            msg_collect.append(tmp_str)
        else:
            isOK['permute'] = True
                
        if ( isOK['min_len'] and isOK['max_len'] and
             isOK['u'] and isOK['l'] and isOK['d'] and isOK['s'] and
             isOK['permute'] ):
            ret_state = PWstate.COMPLEX_OK

        self._fail_reason = "\n".join(msg_collect)

        return ret_state
        
        
    def __repr__(self):   
        char_msg    = {'l':"lower case", 'u':"upper case", 'd':"number", 's':"special (!?#+% etc)"}
        collect = []
        collect.append( "".join( ("Required minimum pass phrase length: ", str(self._min_len) ) ) )
        collect.append( "".join( ("Maximum allowed pass phrase length: ", str(PWdefaults.MAX_LEN) ) ) )
        collect.append( "".join( ("Require ", str(self._char_req['permute']), " out of following character classes" ) ) )
        for chr_cls in ('u', 'l', 'd', 's'):
            collect.append("".join(("For ", char_msg[chr_cls], ", need minimum of ", str(self._char_req[chr_cls]) ," characters" )))
        return ("\n".join(collect))

#######################
### End of PWcomplexity        
#######################  
        
class Pathwell:
    ''' This is an implementation of the topology work from kore logic '''
    ''' Their original is patented, but released publically under a GNU licence '''
    ## need to get details
    
    ## the premise is that human-chosen passwords cluster strongly on patterns
    ## such as Password1, Password1!, Summer16, Summer2016 etc
    ## (see xxxx for a detailed and scary presentation on password distributions)
    
        # should wrap this with os
        ## need to check licence and opensource this section if required
    def __init__(self):
        ###
        pass
    
     
    def convert_ulds_template_to_regexp(self,template):
        collector = []
        for ch in template:
    # ?? unicode issues
            if ('u' == ch.lower() ):
                collector.append("[A-Z]")
            elif ('l' == ch.lower() ):
                collector.append("[a-z]")
            elif ('d' == ch.lower() ):
                collector.append("[0-9]")
            elif ('s' == ch.lower() ):
                collector.append("[^0-9a-zA-Z]")
            else:
                raise ValueError
        return "".join(collector)
    
    def convert_ulds_template_to_hint(  self, template, *, basehint="password1!",
                                        extchar={'u':'X','l':'x','d':'0','s':'!'} ):                         
        try:
            assert ('u' in extchar and 'l' in extchar and 'd' in extchar and 's' in extchar)
            
            collector = []
            
            for i, ch in enumerate(template):
        # ?? unicode issues                     
                if ('u' == ch.lower() ):
                    if (i<len(basehint)):
                        hintchar = basehint[i].upper()
                    else:
                        hintchar = extchar['u']
                elif ('l' == ch.lower() ):
                    if (i<len(basehint)):
                        hintchar = basehint[i].lower()
                    else:
                        hintchar = extchar['l']
                elif ('d' == ch.lower()):
                    if (i<len(basehint)):
                        if (re.match(r'\d+',basehint[i])):
                            hintchar = (basehint[i])
                        else:
                            hintchar = extchar['d']
                    else:
                        hintchar = extchar['d']
                elif ('s' == ch.lower()):
                    if (i<len(basehint)):
                        if (re.match(r'[^0-9a-zA-Z]+',basehint[i])):
                            hintchar = (basehint[i])
                        else:
                            hintchar = extchar['s']
                    else:
                        hintchar = extchar['s']
                else:
                    hintchar = ''
        ## should we warn or error - error would crash change_pw functions
        ## log instead
                    print ("WARN: Unmatchable char {:s} in ulds topology template ({:s})".format(ch,template))
                collector.append(hintchar)
            
            return "".join(collector)
        except:
            raise
        
    def convert_hint_to_ulds_template( self, hint):                         
        try:
            collector = []
            
            for ch in hint:
        # ?? unicode issues                     
                if (ch.islower() ):
                    tempchar = 'l'
                elif (ch.isupper() ):
                    tempchar = 'u'
                elif (ch.isdigit() ):   # particular issue on unicode
                    tempchar = 'd'
                else:
                    tempchar = 's'
                collector.append(tempchar)
            return "".join(collector)
        except:
            raise    
        
    def parse_pattern_blacklist_file(self,blacklist_fname):
        # pattern blacklist has format
        #   # comment/excluded line
        #   ullldds
        #   ullllllds 
        #   ....
        # where u is uppercase, l lowercase, d digit, s special (@#!.[ etc)
        
        ## probably better (for speed etc) to extend format to
        ##      template   regexp   hint
        ## OR how about a self hinting version??
        ##      Password1   regexp
        
        #
        # will default to trying the top 100 list from 2014 in the install tree
        # returns a list of regexp matches
        # 
        ## should work out how to persist this in object
        ## especially if using something like a lambda function
        
        try:
            if os.path.isfile(blacklist_fname):
                with open(blacklist_fname) as f:
                ## logging
                    print ("opened blacklist file OK\n")
                    list_templates = f.readlines()
                for t in list_templates:
                    if re.match(r'^#',t):
                        next
                    else:
                        # convert template to regexp and append to list
                        rgxp_collector = list("^")
                        for ch in t:
                            if ('u' == ch or 'U' == ch):
                                rgxp_collector.append("[A-Z]")
                            if ('l' == ch or 'L' == ch):
                                rgxp_collector.append("[a-z]")
                            if ('d' == ch or 'L' == ch):
                                rgxp_collector.append("[0-9]")
                            if ('s' == ch or 'S' == ch):
                                rgxp_collector.append("[^0-9a-zA-Z]")
                return "".join(rgxp_collector)
            else:
            ## add logging
                print ("WARNING: password topology blacklist ({:s}) not found \n".format(blacklist_fname), file=sys.stderr)
        except:
            raise
        
    def pattern_tests(self,clearpw:string, pattern_blacklist:list, *, ignore_len:int=20):
        # see latest OWASP on topologies and the korelogic work (search pathwell topologies)
        # this is not the full implementation (eg no enforcement of topology change when
        # changing password), and not worked out what to do about hinting
        # ignore_length sets threshold at which we ignore these rules
        #
        # could add functionality for topology class hash to store as well, but would need to 
        # make sure copied stable salt (or extract from hashpw)
        # need to think about return values
        # lets start with a tuple of PWstate and list(failed templates), maybe
        try:
            return_state = enums.PWstate.TMPLT_OK
            list_failed_tmplts = []
            for p in pattern_blacklist:
            
            ## should we only do this for equal length pw??
            
                if re.match(p, clearpw):
                    return_state = enums.PWstate.TMPLT_FAIL
            ####################### THIS IS ONLY FOR TESTING ##########
            ###### Move to log function idc
                    print ("clearpw {:s} fails {:s}".format(clearpw, p))
                    
            return (return_state, )
        except:
            raise
 
 
class Lockout:
 
    _delay_dict = {}
    def __init__(self, delay_dict={0:0}):
        ''' delay_dict = {n_attempts:delay_to_next_in_seconds,....} '''
        ''' gaps in n_attempts will be filled by delay for next lowest '''
        ''' n_attempts or 0 if none. lockoutonMaxAttempts=True '''
        ''' makes delay function return a flag that can be used to '''
        ''' set a disabled flag on an account requiring admin action '''
        ## need to think about max attempts and how to handle account
        ## could do something clever with n_attempts:tuple(delay:action)
        
        if not isinstance(delay_dict,type(self._delay_dict)):
            raise TypeError("Attempt to set Lockout with non-dict type {:s}".format(type(delay_dict)))
        
        for n_attempts in delay_dict.keys():
        
        # could these be duplicated, and have we checked ***********
        
            if not isinstance(type(n_attempts),type(int)):
                raise TypeError("Using non-integer type for n_attempts in setting ",
                        "Lockout {:s}".format(str(type(n_attempts))))
            if isinstance(type(n_attempts), type(0.5)):
                #### logging
                print ("Warning: in Lockout __init__ float value for n_attempts will be rounded")
            if not( 0 <= n_attempts):
                raise ValueError("Negative value {:s} for n_attempts in setting ",
                        "Lockout".format(str(n_attempts)) ) 
            
            # we'll skip over value check while thinking about the tuple point above
                       
        self._delay_dict = delay_dict.copy()
 
    def fetch_lockout(self, n_attempts):
        ## as above, may need to tweak if decide to change return value
        
        if not isinstance(type(n_attempts), type(int)):
            # should this be caught locally to avoid crashing user login??
            # but this is programmatic, so no
            raise TypeError("Attempt to use non-integer type ({:s}) in ",
                        "fetch_lockout".format(type(n_attempts)))
        if isinstance(type(n_attempts), type(0.5) ):
            #### logging
            print ("Warning: float passed in n_attempts to fetch_lockout - will round down")
            raise RuntimeError("Forcing error in debugging for n_attempts = {:s}".format(str(n_attempts)))
        if (n_attempts < 0):
            raise ValueError("Attempt to fetch_lockout with negative ({:s}) ",
                            "n_attempts".format(str(n_attempts)))
            
        # need to make sure we have an appropriately ordered list of dict settings
        # starting from the highest n_attempts
        
        for (n, dly) in sorted(self._delay_dict.items(), reverse = True):
        #### debug
            print ("Testing ",n_attempts," against ",n)
            
            if ( n_attempts >= n):
                dd = dly
                print ("Returning ",dd)
                return dd   # NB this should still be a viable option
                            # if we place an function object in dict
       
class userPW:

    def __init__(self):
        self._defaulthash = b"2b"    # bcrypt, default as of Apr 2016
        self._rounds      = 14       # greater than default of 12, need to tune against system
                                     # could use a pointer to default algorithm here
        self._min_len     = 8        # mininum length
        self._top_blcklst   = "../config/pw_topology_blacklist.txt"
        self._list_regexps  = []     # should load default set here

        