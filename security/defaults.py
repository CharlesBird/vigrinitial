from enum import Enum, unique   # python > 3.4

@unique
class PWstate(Enum):
    FAIL        = 0
    MATCH       = 1
    MIGRATE     = 2
    COMPLEX_OK  = 3
    COMPLEX_FAIL= 4
    TMPLT_OK    = 5
    TMPLT_FAIL  = 6
    
class PWdefaults():
    # ulds are character classes (upper/lower/digit/special)
    # - might amend to for the common specials (., space !) as subtype
    # PERM is number of class permutations required 
    MIN_CHAR    = {'u':1,'l':1,'d':1,'s':1}
    MIN_LEN     = 8
    MAX_LEN     = 1024  # this is to block DDOS
    PERMUTE     = 3