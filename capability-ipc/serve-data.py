#
# basic interprocess communication parsing (analytics to visualisation engine) 
#
# lets try the falcon framework for a quick start-update
# adding builtin wsgiref

## data serving

# standard libraries
from wsgiref.simple_server import make_server, demo_app
import json

# installed dependencies
import falcon

PORT = 8000
HOST = '127.0.0.1'

# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.

## will need to instantiate a handler for each analytic exposed by the server


# not working yet
call tupleResource(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ('\nTwo things awe me most, the starry sky '
                     'above me and the moral law within me.\n'
                     '\n'
                     '    ~ Immanuel Kant\n\n')
    

class ThingsResource(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ('\nTwo things awe me most, the starry sky '
                     'above me and the moral law within me.\n'
                     '\n'
                     '    ~ Immanuel Kant\n\n')

class describeCapabilities(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        capObj = {u"answer": [42.2], u"abs": 42}

        resp.body = json.dumps(capObj)
        
                     
# falcon.API instances are callable WSGI apps
# 'application' enables gunicorn usage
app = application = falcon.API()

# Resources are represented by long-lived class instances
things = ThingsResource()
capabilities = describeCapabilities()

# things will handle all requests to the '/things' URL path
app.add_route('/things', things)
app.add_route('/capabilities',capabilities)

httpd = make_server( HOST, int(PORT), app)
print("Serving HTTP on port ", int(PORT))

# Respond to requests until process is killed
httpd.serve_forever()

