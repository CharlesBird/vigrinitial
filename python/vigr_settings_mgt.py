#! /usr/bin/python
#
# (c) Charles Bird, 2016
#
# connect to raw data sources
# initially start with local file resources, but this will also handle cloud
# data locations (to pass to api backend - not to execute analysis in this module)
#

import logging

# AWS
import boto3
 
# local dependencies
import vigr_utils as vutils

mod_logger=logging.getLogger(__name__)

# local dependencies
import vigr_globals as globals

class user_data():
    
    _uid = None     ## this will part of the basis for brushing
    _location = None
    _auth_obj = None    ## token for cloud
    _owner = None
    _desc = None
    
    
    def __init__(fully_qual_fname, *, desc = None):
        self._uid = uuid.uuid4()
        self._location = fully_qual_fname   # don't check for access here - might be removable
        if desc:
            self.update_description(desc)
    
    def update_location(fully_qual_fname):
        self._location = fully_qual_fname
    
    def update_description(desc):
        ## need to deal with encoding
        self._desc = html_escape(desc)
        
        
        



def main():
    print ("Running ", vutils.get_clean_prog_name(), " as main")
    
if __name__ == "__main__":
    logging.basicConfig(level=globals.preconfig_logging_level)
    mod_logger = logging.getLogger(__name__)
    mod_logger_console= logging.StreamHandler(sys.stdout)
    mod_logger_console.setLevel(logging.DEBUG)
    main()