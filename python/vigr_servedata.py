#
# basic interprocess communication parsing (analytics to visualisation engine) 
#
# lets try the falcon framework for a quick start-update
# adding builtin wsgiref

## data serving

# standard libraries
from wsgiref.simple_server import make_server, demo_app
import json
import os
import sys
import pprint

# installed dependencies    
import falcon   # need this anyway to package responses correctly
from falcon_cors import CORS

# vigr modules
import vigr_vidjil as vjl
import vigr_diversity as vdiv
import vigr_utils as vutil

# REST setup

# merge with config settings

### standalone test server
PORT = 8000
HOST = '127.0.0.1'

# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.


## will need to instantiate a handler for each analytic exposed by the server
## TODO: add try/except blocks


class ping():
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ( '{status:"OK"}' )
        
class listLibrariesAndData():
    def on_get(self, req, resp):
        """Handles GET requests"""
    
        resp_dict = vutil.build_library_dataset_obj()
        resp.status = falcon.HTTP_200
        resp.body = json.dumps( resp_dict )
        
        try:    
            pass
        except: # idc improve
            resp.status = falcon.HTTP_500
            resp.body = json.dumps( '{status:"failed library read"}' )
    
        
class testGet():
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  
        retval = "testGet: "
        if (req.query_string):
            retval += req.query_string
        else:
            retval += "no query string found"
        print(retval)
        resp.body = json.dumps(retval)

class testPostForm():
    def on_post(self, req, resp):
        """Handles POST requests"""
        
        print (req.params)
        
        resp.status = falcon.HTTP_202
        # resp.body = json.dumps(result_json, encoding='utf-8')
        resp.body = json.dumps(req.params)

class morisitaHorn():
    def on_post(self, req, resp):
        """Handles POST request for MH index between two datasets"""
        ''' expects pair of lib/data ids, and group-by variable '''
        
        # we're folding POST body variables into the req.params dict
        # could build these better in front-end

        try:
            lib_ds_pair1 = dict()
            lib_ds_pair2 = dict()
            
            lib_ds_pair1["lib"] = req.params["lib1"]
            lib_ds_pair1["ds"] = req.params["data1"]
            lib_ds_pair2["lib"] = req.params["lib2"]
            lib_ds_pair2["ds"] = req.params["data2"]
            
            desc=lib_ds_pair1["lib"]+"/"+lib_ds_pair1["ds"]+" vs "
            desc +=lib_ds_pair2["lib"]+"/"+lib_ds_pair2["ds"]
            
            if "group" in req.params:
                group_by = req.params["group"]
            else:
                group_by = "IGH"
                print ("WARNING: defaulting morisitaHorn to use group_by=", group_by)
 
            if False:    # debug
                print("MH handler input parameters")
                print (lib_ds_pair1)
                print (lib_ds_pair2)
                print (group_by)
                
            retval = vjl.vigr_mh_map( lib_ds_pair1, lib_ds_pair2, group_by, desc)
            resp.status = falcon.HTTP_200
            resp.body = retval
        except:
            e = sys.exc_info()[0]
            print("ERROR: MH handler failed: ",e)
            resp.status = falcon.HTTP_500
            resp.body = "Problem accessing data for Morisita-Horn calculations"
        

        
class diversityProfile():
    def on_post(self, req, resp, lib_id, data_id):
        """Handles POST requests"""
        
        vidjil_clones_ext = ".vidjil"
        data_dir = "C:\\Users\\Charlie\\Documents\\vigrinitial\\pipeline_results\\"    # pick this up from config environment
                                                                 # need to watch out for path.sep issues
                                                                 # and escape final \ in windows
            
        # idc incorporate cache behaviour here
        # and pipeline automation - currently this relies on the existence of a .vidjil file
        
        # build filename to vidjil data idc make utility function
               
        fname = data_id+vidjil_clones_ext
        tgt_file = os.path.join(data_dir, lib_id)
        tgt_file = os.path.join(tgt_file, fname)
        
        if False:   # debug
            print ("library = ", lib_id)
            print ("dataset = ", data_id)
            print ("params = ", req.params)
            print("using ",tgt_file)   ######
        
        try:        
            clonotypes = vjl.vidjil( tgt_file )
            pop = vdiv.Population( clonotypes.extract_clones_by_count(), vdiv.ObjContains.MEMBERS)
            json_obj = pop.vigr_div_profile( -1, 5, 0.5, "spiked population" )
## need to package into vigrMsg TODO
            resp.status = falcon.HTTP_200
            resp.body = json_obj
        except:
            resp.status = falcon.HTTP_500
            resp.body = "File "+tgt_file+" not found"
        

class returnJSON ( ):
    # vigrObj contains an adapter and a data element
    def on_get(self, req, resp):
        """Handles GET requests"""
        
        # req will key into library and dataset
        
        resp.status = falcon.HTTP_200  # This is the default status
        capObj = {u"answer": [42.2], u"abs": 42}

        resp.body = json.dumps(capObj)

# don't need this here in 1st iteration - can hold in frontend
# add a library from presented list to active
# idc needs validation by authentication and location (ie that can't change id to hunt through data)
class useLibrary ():
    ##def on_get(self, req, resp):
    ##    resp.body= json.dumps (req.keys() )
    ##    resp.status = falcon.HTTP_200   
    
    def on_post(self, req, resp):
        try:
            resp.body= json.dumps (req.context)
           #libID = req.context['libraryID']
        except KeyError:
            raise falcon.HTTPBadRequest(
                'Missing libraryID',
                'A libraryID must be submitted in the request body.')

        #proper_thing = self.db.add_thing(doc)

        resp.status = falcon.HTTP_201
        #resp.location = '/useLibrary/%s' % (user_id, proper_thing['id'])

class describeDataset ():
    def on_get(self, req, resp, id):
        dataset = {};
        dataset["1"] = "Rat"
        dataset["2"] = "Human"
        
        respStr = "Library "+str(id)+" contains "+dataset[id]+" samples"
        resp.body= json.dumps ( respStr )
        resp.status = falcon.HTTP_200   
        

class ThingsResource(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ('\nTwo things awe me most, the starry sky '
                     'above me and the moral law within me.\n'
                     '\n'
                     '    ~ Immanuel Kant\n\n')

## idc can these be subclassed from more general verb handlers??
class describeCapabilities(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        capObj = {u"answer": [42.2], u"abs": 42}

        resp.body = json.dumps(capObj)


## make a wrapper object for REST server in case change framework
##
class RestSvr():
    # set up CORS access for all sites
    # restrict_cors = CORS(allow_all_origins = False)
    cors = public_cors = CORS(allow_all_origins = True)
     
    def __init__(self):
        self.app = falcon.API(middleware=[self.cors.middleware])
        self.app.req_options.auto_parse_form_urlencoded = True  # parse forms into query string parameters
        # falcon.API instances are callable WSGI apps
        # using 'application' enables gunicorn usage, but using built-in http.server for proof of concept
        
    def add_route (self, path, handlerClsInstance):
        self.app.add_route( path, handlerClsInstance)
        
        
def configure_routes ():    # can move to passing dict with route/function map
    pass

# standalone test server        
def main():                     
    rest = RestSvr()
    
    # capabilities = describeCapabilities()
    rest.add_route('/capabilities', describeCapabilities() )    # creating a route with a dynamic object
                                                                # test for efficiency in completed version
                                                                # maybe a dict with static instances??
    
        # debug routines
    rest.add_route('/testPostForm', testPostForm () )
    rest.add_route('/testGet', testGet () )
        
    # Vigr routines
    rest.add_route('/ping', ping() )    # lets front end display appropriate message
                                        # if can't connect to control server

    rest.add_route('/fetchLibraryData', listLibrariesAndData() )
    rest.add_route('/describeDataset/{id}', describeDataset() ) ## superceded by desc values in LibDataObj
    rest.add_route('/diversityProfile/{lib_id}/{data_id}', diversityProfile())

    # add route to do Morisita-Horn on two data sets
    # need to reflect data layout advice in manual to make this intuitive
    rest.add_route('/morisitaHorn/', morisitaHorn()) # have to POST pairs of lib/data as parameters
    
    
    # fire up a basic server - do not expose this to the internet
    httpd = make_server( HOST, int(PORT), rest.app )
    print("Serving HTTP on port ", int(PORT))

    ## Respond to requests until process is killed
    httpd.serve_forever()

if ( __name__ == "__main__" ):
    print ("vigr_servedata running test functions")
    main()

    
