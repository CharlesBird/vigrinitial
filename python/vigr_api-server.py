#
# WSGI interface for visualisation engine - backend comms
# 
# (c) Charles Bird , 2016
# 
# Based on the Falcon REST framework, with the falcon_cors extension
# for cross-origin management
#
# using the reference wsgi implementation, can change for external server
#
#

# standard libraries
from wsgiref.simple_server import make_server
import json
import os
import csv

# installed dependencies
import falcon
from falcon_cors import CORS
import vigr_utils as vutils


PORT = 8000
HOST = '127.0.0.1'

# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.

# make a middleware layer to manage CORS for cross-origin access

#restrict_cors = CORS(allow_origins_list=['localhost'])
restrict_cors = CORS(allow_all_origins = False)
cors = public_cors = CORS(allow_all_origins = True)

# falcon.API instances are callable WSGI apps
# alias to application for gunicorn support

#app = application = falcon.API(media_type="application/json; charset=UTF-8", middleware=[cors.middleware])
app = application = falcon.API( middleware=[cors.middleware])

class ThingsResource(object):
    def on_get(self, req, resp):
 
        # test blocking CORS - XXXXX not working as expected TODO
        cors = public_cors
 
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ('\nTwo things awe me most, the starry sky '
                     'above me and the moral law within me.\n'
                     '\n'
                     '    ~ Immanuel Kant\n\n')

class describeCapabilities(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        capObj = {u"answer": [42.2], u"abs": 42}

        resp.body = json.dumps(capObj)

class graphResources(object):
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        
        ## dummy example - this would be populated from the module store
        graphObj = { "graph": {
                        u"graph_name": "xy", 
                        u"data_type_reqt": "xy",
                        u"d3_module": "xy"
                        }
                    }

        resp.body = json.dumps(graphObj)

        
class xySource(object):

    def __init__(self, data_dir):
        self.storage_path = data_dir
    
    def on_get(self, req, resp, data_id):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = 'application/json'
                
        ## this will be read from a csv fed by an selector in url
        data_fname = "".join([data_id, ".csv"])
        data_file = os.path.join(self.storage_path, data_fname)
        
        with open(data_file, newline='', encoding='utf-8') as csvfile:
                reader = csv.reader(csvfile)
                xyObj= json.dumps( [ row for row in reader ])
        
        ### need to catch errors
        
        # debug - checking get to here
        # xyObj = {u"data_id for csv": data_id} 
        # resp.body = json.dumps(xyObj)
        resp.body = xyObj
                   


# Resources are represented by long-lived class instances
things = ThingsResource()
capabilities = describeCapabilities()
xy = xySource(vutils.find_global_data_dir())
graphs = graphResources()       # should do the get/post split on collection/items


# things will handle all requests to the '/things' URL path
app.add_route('/things', things)
app.add_route('/capabilities',capabilities)
app.add_route('/xy/{data_id}', xy)
app.add_route('/list_graphs', graphs)


//  remote connections need ssl
// ideally we should provide cert for this connection, but would only be
// self-signed, which would upset most browsers
httpd = make_server( HOST, int(PORT), app)
print("Serving HTTP on port ", int(PORT))

# Respond to requests until process is killed
httpd.serve_forever()

