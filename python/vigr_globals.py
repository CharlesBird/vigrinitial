### shared globals


import logging  # need for defined levels

## these are hardwired and can only be changed here
##
default_cfg_fname   = "default.cfg"
logging_cfg_fname   = "logging.cfg"
root_logger_name    = "VIgR"
cmd_ln_sect         = "COMMAND_LINE_OPTIONS"
cmd_ln_help         = "COMMAND_LINE_HELP"
update_src          = "vigr.developers@gmail.com"
run_debug_code      = 1     ### 0 = off, +ve values enable 
public_api_url      = "https://7vtk9rsczh.execute-api.eu-west-1.amazonaws.com/"
public_version_api  = "getCurrentVersions"

### look at OWASP advice etc to set these to sensible values
json_valid_hdrs = {
    "content-type":"application\/json"
}


## this only configures the early logging calls
## before being overriden by the logging.cfg file
#preconfig_logging_level = logging.DEBUG
preconfig_logging_level = logging.WARN

## the following can be overridden via cfg file or command line
default_api_addr    = "localhost"
default_api_port    = 8000


    