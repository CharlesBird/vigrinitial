#
# config setting parser
#
#

import configparser 
import os
import sys
import logging

import vigr_globals as globals
import vigr_exceptions as vx


# can't create module level VIgR logger here, because the VIgR logger
# is not instantiated until after the global defaults are loaded
#
##logger_tag = "".join([ globals.root_logger_name, ".config" ] )
##mod_logger = logging.getLogger(logger_tag)
##mod_logger.debug("Created config module logger")
mod_logger = logging.getLogger(__name__)

class config_obj():
    ''' read a configuration file in .INI style into an object '''
    _parser = None
    
    ## this dict only needs contain essential values
    ## new ones will be added by parsers
    _config = {
        "port":globals.default_api_port,
        "host":globals.default_api_addr
        }
        
#        "protocol":'http://'    ## running local api will only support https with self-signed cert
#                                ## need to think about pros and cons vs browser support
#
        
    
    def __init__(self):
        if not self._parser:
            self._parser = configparser.ConfigParser()
        
    def load_config_file(self, config_fname):
        try:
            with open(config_fname) as config_file:
                mod_logger.debug("opened default config file (%s) successfully", config_fname)
                self._parser.read_file(config_file)
                mod_logger.debug("read config file")
                
                if globals.run_debug_code:
                    mod_logger.debug("Parsed values follow: ")
                    mod_logger.debug("===================== ")
                    for s in self.list_config_sections():
                        mod_logger.debug(s)    ## need to watch out for untrusted input in user file
                        mod_logger.debug("-------------")
                        options=self.map_config_section(s)
                        mod_logger.debug(options)
                        
        except IOError as e:
            log_str = " ".join(["Failed to open ", str(config_fname)])
            ## NB remember to use the basic logging stream here
            mod_logger.error(log_str)
            raise vx.configOpenError(config_fname)
        except:
            e = sys.exc_info()[0]
            mod_logger.error("can't parse default config file (%s) because (%s) ", config_fname, e)
            raise vx.configParseError(config_fname)
            ### add reversion to hard coded options, but only for default fname **
        
    def list_config_sections(self):
        # modify this to display defaults section if extant
        return self._parser.sections()
        
    def map_config_section(self, section):
        sect_opts = {}    ## default dict to pass
        
        ##  TODO handle non-exisiting section 
        options = self._parser.options(section)
        for option in options:
            try:
                sect_opts[option] = self._parser.get(section, option)
                mod_logger.debug("".join([ str(option)," = ",str( sect_opts[option]) ]))
            except:
            # bit buggy; says ok with x =, but just x gives errors for all options and throws err on reading file
                mod_logger.info("".join(["Missing value for ",str(option), "in section ".str(section)]))
                sect_opts[option] = None
        return sect_opts
        
    def update_config(self, config_dict):
        
        for opt in config_dict:
            self._config[opt] = config_dict[opt]

    def fetch_value_for(self, opt_name):        ## check on creation on search issue TODO
        if opt_name in self._config:
            return self._config[opt_name]
        else:
            return None
  
def readConfigSectionByName(section_name):
    # need to be careful about shallow/deep here and tracking modifications
    pass
 
   
# how best to do the options dict of dict ??
# how to manage users
# does this need a debug/log object -- yes otherwise this has to be done on a per section basis
def readLogSettings():
    pass
    
def updateLogSettings():
    pass
    
    
    
    

   