#! /usr/bin/python
#
# (c) Charles Bird, 2016
#
# manage user authentication
# for local users, this is clearly nugatory
# (in the sense anyone can edit the source code to skip the check)
# but put the interface in place to make later additions easier
# and to support fork of local/server branches

import logging

# AWS
import boto3
 
# local dependencies
import vigr_utils as vutils

mod_logger=logging.getLogger(__name__)

# local dependencies
import vigr_globals as globals

// what kind of object should we generate here

class user_auth():
    
    def __init__():
        pass
    
    def authenticate(user_name, password):
        pass
    
    
    
def main():
    print ("Running ", vutils.get_clean_prog_name(), " as main")
    
    
if __name__ == "__main__":
    logging.basicConfig(level=globals.preconfig_logging_level)
    mod_logger = logging.getLogger(__name__)
    mod_logger_console= logging.StreamHandler(sys.stdout)
    mod_logger_console.setLevel(logging.DEBUG)
    main()